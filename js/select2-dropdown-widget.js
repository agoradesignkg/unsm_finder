/**
 * @file
 * Transforms links into a Select2 enabled dropdown list.
 */

(function ($) {

  'use strict';

  Drupal.unsm_finder = Drupal.unsm_finder || {};
  Drupal.behaviors.facetsSelect2DropdownWidget = {
    attach: function (context, settings) {
      Drupal.unsm_finder.makeDropdown(context, settings);
    }
  };

  /**
   * Turns all facet links into a dropdown with options for every link.
   */
  Drupal.unsm_finder.makeDropdown = function (context, settings) {
    let items = once('facets-select2-dropdown-transform', '.finderfacet--type-select', context);
    // Find all dropdown facet links and turn them into an option.
    $(items).each(function () {
      var $ul = $(this).find('.facet__items');
      var $links = $ul.find('.facet__item a');
      var $dropdown = $('<select class="facets-select2-dropdown" />').data($ul.data());

      var id = $(this).data('drupal-facet-id');
      var default_option_label = settings.facets.select2_dropdown_widget[id]['facet-default-option-label'];
      // Add empty text option first.
      var $default_option = $('<option />')
        .attr('value', '')
        .text(default_option_label);
      $dropdown.append($default_option);

      var has_active = false;
      $links.each(function () {
        var $link = $(this);
        var active = $link.hasClass('facet__item__link--active');
        var $option = $('<option />')
          .attr('value', $link.attr('href'))
          .data($link.data());
        if (active) {
          has_active = true;
          // Set empty text value to this link to unselect facet.
          $default_option.attr('value', $link.attr('href'));

          $option.attr('selected', 'selected');
          $link.find('.js-facet-deactivate').remove();
        }
        $option.html($link.text());
        $dropdown.append($option);
      });

      // Go to the selected option when it's clicked.
      $dropdown.on('change.facets', function () {
        window.location.href = $(this).val();
      });

      // Append empty text option.
      if (!has_active) {
        $default_option.attr('selected', 'selected');
      }

      // Replace links with dropdown.
      $ul.after($dropdown).remove();

      // Convert to Select2.;
      $dropdown.css('width', '100%')// http://stackoverflow.com/questions/14313001/select2-not-calculating-resolved-width-correctly-if-select-is-hidden
        .select2({
          templateResult: renderSelectOption,
          templateSelection: renderSelectOption
        });

      Drupal.attachBehaviors($dropdown.parent()[0], Drupal.settings);
    });
  };

  // Select2 template callback.
  // -----------------------------
  function renderSelectOption(item) {
    var output = '';
    var tnPath = jQuery(item.element).data('tn');
    var classAttr = '';
    if (tnPath) {
      output += '<span class="tn-cn"><img class="tn" src="' + tnPath + '"/></span> ';
      classAttr = 'has-tn';
    }
    else {
      classAttr = 'no-tn';
    }
    output += '<span class="item-label ' + classAttr + '">' + item.text + '</span>';
    return jQuery(output);
  }

})(jQuery);
