/**
 * @file
 * Javascript behaviors for Select2 integration.
 */

(function ($, Drupal) {

  'use strict';

  // @see https://select2.github.io/options.html
  Drupal.unsm_finder = Drupal.unsm_finder || {};
  Drupal.unsm_finder.select2 = Drupal.unsm_finder.select2 || {};
  Drupal.unsm_finder.select2.options = Drupal.unsm_finder.select2.options || {};

  /**
   * Initialize Select2 support.
   *
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.unsinnSelect2 = {
    attach: function (context) {
      if (!$.fn.select2) {
        return;
      }

      let items = once('unsinn-select2', 'select.js-unsinn-select2, .js-unsinn-select2 select', context);
      $(items)
        // http://stackoverflow.com/questions/14313001/select2-not-calculating-resolved-width-correctly-if-select-is-hidden
        .css('width', '100%')
        .select2(Drupal.unsm_finder.select2.options);
    }
  };

})(jQuery, Drupal);
