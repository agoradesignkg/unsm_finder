/**
 * @file
 * Transforms links into a checkboxes.
 */

(function ($) {

  'use strict';

  Drupal.unsm_finder = Drupal.unsm_finder || {};
  Drupal.behaviors.facetsCheckboxesWidget = {
    attach: function (context, settings) {
      Drupal.unsm_finder.makeCheckboxes(context, settings);
    }
  };

  /**
   * Turns all facet links into a checkboxes.
   */
  Drupal.unsm_finder.makeCheckboxes = function (context, settings) {
    // Find all checkbox facet links and give them a checkbox.
    let links = once('facets-checkbox-transform', '.finderfacet--type-checkboxes a.facet__item__link', context);
    links.forEach(Drupal.unsm_finder.makeCheckbox);
    // Set indeterminate value on parents having an active trail.
    // $('.facet-item--expanded.facet-item--active-trail > input').prop('indeterminate', true);
  };

  /**
   * Replace a link with a checked checkbox.
   */
  Drupal.unsm_finder.makeCheckbox = function (elem) {
    var $link = $(elem);
    var active = $link.hasClass('facet__item__link--active');
    var description = $link.html();
    var href = $link.attr('href');
    var id = $link.data('drupal-facet-item-id');

    var checkbox = $('<input type="checkbox" class="facets-checkbox">')
        .attr('id', id)
        .data($link.data())
        .data('facetsredir', href);
    var label = $('<label for="' + id + '">' + description + '</label>');

    checkbox.on('change.facets', function (e) {
      Drupal.unsm_finder.disableFacet($link.parents('.facet__items'));
      $(this).siblings('a')[0].click();
    });

    if (active) {
      checkbox.attr('checked', true);
      // label.find('.js-facet-deactivate').remove();
    }

    $link.before(checkbox).before(label).hide();
  }

  /**
   * Disable all facet checkboxes in the facet and apply a 'disabled' class.
   *
   * @param {object} $facet
   *   jQuery object of the facet.
   */
  Drupal.unsm_finder.disableFacet = function ($facet) {
    $facet.addClass('facets-disabled');
    $('input.facets-checkbox', $facet).click(Drupal.unsm_finder.preventDefault);
    $('input.facets-checkbox', $facet).attr('disabled', true);
  };

  /**
   * Event listener for easy prevention of event propagation.
   *
   * @param {object} e
   *   Event.
   */
  Drupal.unsm_finder.preventDefault = function (e) {
    e.preventDefault();
  };

})(jQuery);
