/**
 * @file
 * Provides the exact slider functionality.
 */
(function ($) {

  'use strict';

  Drupal.unsm_finder = Drupal.unsm_finder || {};

  Drupal.behaviors.facet_slider_exact = {
    attach: function (context, settings) {
      if (settings.facets !== undefined && settings.facets.exact_sliders !== undefined) {
        $.each(settings.facets.exact_sliders, function (facet, settings) {
          Drupal.unsm_finder.addSlider(facet, settings);
        });
      }
    }
  };

  Drupal.unsm_finder.addSlider = function (facet, settings) {
    var defaults = {
      onFinish: function (data) {
        window.location.href = settings.url.replace('__range_slider_min__', data.from_value).replace('__range_slider_max__', data.to_value);
      }
    };

    $.extend(defaults, settings);

    $('#' + facet).ionRangeSlider(defaults);
  };

})(jQuery);
