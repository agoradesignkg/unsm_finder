<?php

namespace Drupal\unsm_finder;

use Drupal\unsm_finder\Form\Value\AccessoryQuantitySelection;
use Drupal\unsm_finder\Trailer\RemoteTrailer;
use Drupal\Core\TempStore\PrivateTempStoreFactory;

class RemoteTrailerAccessorySelectionStore implements RemoteTrailerAccessorySelectionStoreInterface {

  /**
   * The private temp store object.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStore
   */
  protected $privateTempStore;

  /**
   * Constructs a new TrailerSelectionStore object.
   *
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $private_temp_store_factory
   *   The private temp store factory.
   */
  public function __construct(PrivateTempStoreFactory $private_temp_store_factory) {
    $this->privateTempStore = $private_temp_store_factory->get('uns_trailer');
  }

  /**
   * @inheritDoc
   */
  public function selectAccessories(RemoteTrailer $trailer, array $accessories) {
    $store_key = $this->buildKey($trailer);
    $values = [];
    /** @var \Drupal\unsm_finder\Form\Value\AccessoryQuantitySelection $accessory */
    foreach ($accessories as $accessory) {
      $values[] = $accessory->toArray();
    }
    $this->privateTempStore->set($store_key, $values);
  }

  /**
   * @inheritDoc
   */
  public function updateQuantity(RemoteTrailer $trailer, AccessoryQuantitySelection $accessory, $strict = TRUE) {
    $changed = FALSE;
    $selected_accessories = $this->loadSelectedAccessories($trailer);
    foreach ($selected_accessories as $idx => $selected_accessory) {
      if ($selected_accessory->getSparepartId() === $accessory->getSparepartId()) {
        // Replace the accessory.
        $selected_accessories[$idx] = $accessory;
        $changed = TRUE;
        break;
      }
    }
    if (!$changed && !$strict) {
      $selected_accessories[] = $accessory;
      $changed = TRUE;
    }
    if ($changed) {
      $this->selectAccessories($trailer, $selected_accessories);
    }
  }

  /**
   * @inheritDoc
   */
  public function loadSelectedAccessories(RemoteTrailer $trailer) {
    $values = $this->loadSelectedAccessoriesRaw($trailer);
    $selected_accessories = [];
    foreach ($values as $value) {
      $selected_accessories[] = AccessoryQuantitySelection::fromArray($value);
    }
    return $selected_accessories;
  }

  /**
   * @inheritDoc
   */
  public function loadSelectedAccessoriesRaw(RemoteTrailer $trailer) {
    $store_key = $this->buildKey($trailer);
    $values = $this->privateTempStore->get($store_key);
    return $values ?: [];
  }

  /**
   * Builds the key to be used in the temp store for the given trailer.
   *
   * @param \Drupal\unsm_finder\Trailer\RemoteTrailer $trailer
   *   The trailer entity.
   *
   * @return string
   *   The key to be used in the temp store for the given trailer.
   */
  protected function buildKey(RemoteTrailer $trailer) {
    return 'selected_trailer:' . $trailer->getId();
  }

}
