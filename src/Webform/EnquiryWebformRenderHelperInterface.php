<?php

namespace Drupal\unsm_finder\Webform;

use Drupal\webform\WebformSubmissionInterface;

/**
 * Defines the enquiry webform render helper interface.
 */
interface EnquiryWebformRenderHelperInterface extends NormalizeCountryInterface {

  /**
   * Extracts the personal data from the given (enquiry) webform submission.
   *
   * NOTE: that this is solely written for usage with webform submissions of
   * type 'enquiry'. Similar structured webforms may produce similar results,
   * but this isn't intended.
   *
   * @param \Drupal\webform\WebformSubmissionInterface $enquiry
   *   The webform submission entity (of bundle 'enquiry').
   *
   * @return string[]
   *   The submitted personal data as array, having the labels as key and the
   *   data as values.
   */
  public function getPersonalData(WebformSubmissionInterface $enquiry);

  /**
   * Extracts the address data from the given (enquiry) webform submission.
   *
   * NOTE: that this is solely written for usage with webform submissions of
   * type 'enquiry'. Similar structured webforms may produce similar results,
   * but this isn't intended.
   *
   * @param \Drupal\webform\WebformSubmissionInterface $enquiry
   *   The webform submission entity (of bundle 'enquiry').
   *
   * @return string[]
   *   The submitted address data as array, having the labels as key and the
   *   data as values.
   */
  public function getAddressData(WebformSubmissionInterface $enquiry);

  /**
   * Extracts the selected trailer data from the given webform submission.
   *
   * NOTE: that this is solely written for usage with webform submissions of
   * type 'enquiry'. Similar structured webforms may produce similar results,
   * but this isn't intended.
   *
   * @param \Drupal\webform\WebformSubmissionInterface $enquiry
   *   The webform submission entity (of bundle 'enquiry').
   *
   * @return \Drupal\trailer\Entity\TrailerInterface|null
   *   The selected trailer entity - or NULL, if not available.
   */
  public function getSelectedTrailer(WebformSubmissionInterface $enquiry);

  /**
   * Returns the selected trailer from the given submission as render array.
   *
   * NOTE: that this is solely written for usage with webform submissions of
   * type 'enquiry'. Similar structured webforms may produce similar results,
   * but this isn't intended.
   *
   * @param \Drupal\webform\WebformSubmissionInterface $enquiry
   *   The webform submission entity (of bundle 'enquiry').
   *
   * @return array
   *   A render array, representing the selected trailer from the given enquiry
   *   webform submission.
   */
  public function getSelectedTrailerRenderable(WebformSubmissionInterface $enquiry);

  /**
   * Extracts the selected accessory data from the given webform submission.
   *
   * NOTE: that this is solely written for usage with webform submissions of
   * type 'enquiry'. Similar structured webforms may produce similar results,
   * but this isn't intended.
   *
   * @param \Drupal\webform\WebformSubmissionInterface $enquiry
   *   The webform submission entity (of bundle 'enquiry').
   *
   * @return \Drupal\unsm_finder\Form\Value\AccessoryQuantitySelection[]
   *   The selected accessories (sparepart entities).
   */
  public function getSelectedAccessories(WebformSubmissionInterface $enquiry);

  /**
   * Returns the selected accessories from the given submission as render array.
   *
   * NOTE: that this is solely written for usage with webform submissions of
   * type 'enquiry'. Similar structured webforms may produce similar results,
   * but this isn't intended.
   *
   * @param \Drupal\webform\WebformSubmissionInterface $enquiry
   *   The webform submission entity (of bundle 'enquiry').
   *
   * @return array
   *   A render array, representing the selected accessories from the given
   *   enquiry webform submission.
   */
  public function getSelectedAccessoriesRenderable(WebformSubmissionInterface $enquiry);

}
