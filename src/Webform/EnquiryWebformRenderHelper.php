<?php

namespace Drupal\unsm_finder\Webform;

use Drupal\unsm_finder\Client\UnsinnTrailerDetailsClientInterface;
use Drupal\unsm_finder\Form\Value\AccessoryQuantitySelection;
use Drupal\webform\WebformSubmissionInterface;
use Psr\Log\LoggerInterface;

/**
 * Default enquiry webform render helper implementation.
 */
class EnquiryWebformRenderHelper implements EnquiryWebformRenderHelperInterface {

  use NormalizeCountryTrait;

  /**
   * The UNSINN trailer details client.
   *
   * @var \Drupal\unsm_finder\Client\UnsinnTrailerFinderClientInterface
   */
  protected $client;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Static cache for remote trailers.
   *
   * @var \Drupal\unsm_finder\Trailer\RemoteTrailer[]
   */
  protected $trailerCache;

  /**
   * Constructs a new EnquiryWebformRenderHelper object.
   *
   * @param \Drupal\unsm_finder\Client\UnsinnTrailerDetailsClientInterface $client
   *   The UNSINN trailer finder client.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.
   */
  public function __construct(UnsinnTrailerDetailsClientInterface $client, LoggerInterface $logger) {
    $this->client = $client;
    $this->logger = $logger;
    $this->trailerCache = [];
  }

  /**
   * {@inheritdoc}
   */
  public function getPersonalData(WebformSubmissionInterface $enquiry) {
    // Should we translate them, or fetch from the underlying webform entity?
    $webform_fields = [
      'salutation' => 'Anrede',
      'title' => 'Titel',
      'first_name' => 'Vorname',
      'last_name' => 'Nachname',
      'company' => 'Firma',
    ];
    return $this->getData($enquiry, $webform_fields);
  }

  /**
   * {@inheritdoc}
   */
  public function getAddressData(WebformSubmissionInterface $enquiry) {
    // Should we translate them, or fetch from the underlying webform entity?
    $webform_fields = [
      'street' => 'Straße',
      'concat:postal_code:locality' => 'PLZ / Ort',
      'country' => 'Land',
      'phone' => 'Telefon',
      'email' => 'E-Mail',
    ];
    return $this->getData($enquiry, $webform_fields);
  }

  /**
   * {@inheritdoc}
   */
  public function getSelectedTrailer(WebformSubmissionInterface $enquiry) {
    $trailer_id = intval($enquiry->getElementData('trailer'));
    if (empty($trailer_id) || $trailer_id < 1) {
      return NULL;
    }
    return $this->getRemoteTrailer($trailer_id);
  }

  /**
   * {@inheritdoc}
   */
  public function getSelectedTrailerRenderable(WebformSubmissionInterface $enquiry) {
    $trailer = $this->getSelectedTrailer($enquiry);
    if (empty($trailer)) {
      return [];
    }
    return [
      '#theme' => 'remote_trailer_email',
      '#trailer' => $trailer,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getSelectedAccessories(WebformSubmissionInterface $enquiry) {
    $accessories = (string)$enquiry->getElementData('accessories');
    if (empty($accessories)) {
      return [];
    }
    $accessories = unserialize($accessories);
    if (!is_array($accessories)) {
      $this->logger->warning('Invalid serialized accessories data in webform submission ID %sid', ['%sid' => $enquiry->id()]);
      return [];
    }
    $result = [];
    foreach ($accessories as $accessory) {
      $result[] = AccessoryQuantitySelection::fromArray($accessory);
    }
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function getSelectedAccessoriesRenderable(WebformSubmissionInterface $enquiry) {
    $accessories = $this->getSelectedAccessories($enquiry);
    if (empty($accessories)) {
      return [];
    }

    $table = [
      '#type' => 'table',
      '#attributes' => ['class' => ['accessory-table']],
    ];

    $trailer = $this->getSelectedTrailer($enquiry);

    foreach ($accessories as $row_no => $accessory_selection) {
      $accessory = $trailer ? $trailer->getAccessoryById($accessory_selection->getSparepartId()) : NULL;

      if ($accessory && $accessory->getThumbnail()) {
        $table[$row_no]['image'] = [
          '#theme' => 'accessory_image',
          '#thumbnail' => $accessory->getThumbnail() ? $accessory->getThumbnail()->toArray() : [],
          '#zoomed' => [],
          '#item_id' => $row_no,
        ];
      }
      else {
        $table[$row_no]['image'] = [];
      }

      if ($accessory) {
        $info_renderable = [
          '#theme' => 'accessory_title',
          '#sku' => $accessory->getSkuFormatted(),
          '#name' => $accessory->getTitle(),
        ];
        $table[$row_no]['info'] = $info_renderable;
      }
      else {
        $table[$row_no]['info'] = [
          '#markup' => '<p>Information zu diesem Zubehörteil derezeit nicht verfügbar.</p>',
        ];
      }

      $table[$row_no]['qty'] = [
        '#markup' => $accessory_selection->getQty(),
      ];
    }

    return $table;
  }

  /**
   * Helper function to retrieve values from the given webform submission.
   *
   * @param \Drupal\webform\WebformSubmissionInterface $enquiry
   *   The webform submission entity.
   * @param string[] $webform_fields
   *   An array of webform fields, keyed by field name, having the labels as
   *   value. Special concatenation keys are supported: if the key starts with
   *   'concat:', then multiple key names may follow, separated by ':'.
   *
   * @return string[]
   *   The submitted data as array, having the labels as key and the data as
   *   values.
   */
  protected function getData(WebformSubmissionInterface $enquiry, array $webform_fields) {
    $data = [];
    foreach ($webform_fields as $field => $label) {
      if (substr($field, 0, 7) == 'concat:') {
        $field_names = explode(':', $field);
        array_shift($field_names);
        $values = [];
        foreach ($field_names as $field_name) {
          $single_value = $enquiry->getElementData($field_name);
          if (!empty($single_value)) {
            $values[] = $single_value;
          }
        }
        $value = implode(' ', $values);
      }
      else {
        $value = $enquiry->getElementData($field);
      }

      if (!empty($value)) {
        $data[$label] = $value;
      }
    }
    return $data;
  }

  /**
   * Loads the remote trailer by ID, using static cache.
   *
   * @param $trailer_id
   *   The remote trailer ID.
   *
   * @return \Drupal\unsm_finder\Trailer\RemoteTrailer|null
   *   The remote trailer, if found.
   */
  protected function getRemoteTrailer($trailer_id) {
    if (!isset($this->trailerCache[$trailer_id])) {
      $trailer_details = $this->client->getTrailerDetails($trailer_id);
      if (empty($trailer_details)) {
        return NULL;
      }
      $this->trailerCache[$trailer_id] = $trailer_details;
    }
    return $this->trailerCache[$trailer_id];
  }

}
