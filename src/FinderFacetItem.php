<?php

namespace Drupal\unsm_finder;

use Drupal\Core\Url;

/**
 * Value object representing a single finder facet item.
 */
final class FinderFacetItem {

  /**
   * The value.
   *
   * @var mixed
   */
  protected $value;

  /**
   * The label.
   *
   * @var string
   */
  protected $label;

  /**
   * The url.
   *
   * @var \Drupal\Core\Url
   */
  protected $url;

  /**
   * Whether the item is active (selected).
   *
   * @var bool
   */
  protected $active;

  /**
   * The counter (optional, not present in every facet).
   *
   * @var int|null
   */
  protected $count;

  /**
   * The thumbnail image url.
   *
   * @var string|null
   */
  protected $thumbnail;

  /**
   * Factory method, instantiating a new FinderFacetItem object.
   *
   * The array structure (of the items) is based on the expectations of the
   * structure returned by UNSINN trailer finder REST web service.
   *
   * @param array $values
   *   The facet item as raw value array.
   *
   * @return static
   *   A new FinderFacetItem object, instantiated based on the given values.
   */
  public static function fromArray(array $values) {
    $result = new static();

    if (isset($values['values']['value'])) {
      $result->value = $values['values']['value'];
    }
    $result->label = isset($values['values']['label']) ? $values['values']['label'] : $result->value;
    if (!empty($values['url'])) {
      $result->url = $values['url'] instanceof Url ? $values['url'] : Url::fromUri($values['url']);
    }
    $result->active = !empty($values['values']['active']);

    if (isset($values['values']['count'])) {
      $result->count = $values['values']['count'];
    }

    if (isset($values['values']['thumbnail'])) {
      $result->thumbnail = $values['values']['thumbnail'];
    }
    return $result;
  }

  /**
   * Returns an array representation of this object.
   *
   * Main purpose is to have an array to access to in our Twig template. It is
   * not a render array per se, but all components are either strings, render
   * arrays or at least compatible to be renderable.
   *
   * @return array
   *   An array representation of this object.
   */
  public function toArray() {
    return [
      'value' => $this->value,
      'label' => $this->label,
      'url' => $this->url,
      'count' => $this->count,
      'active' => $this->active,
      'thumbnail' => $this->thumbnail,
    ];
  }

  /**
   * Whether the facet is actively selected.
   *
   * @return bool
   *   TRUE, if the facet is currently active, FALSE otherwise.
   */
  public function isActive() {
    return $this->active;
  }

  /**
   * Get the url.
   *
   * @return \Drupal\Core\Url
   *   The url.
   */
  public function getUrl() {
    return $this->url;
  }

  /**
   * Get the value.
   *
   * @return mixed
   *   The value.
   */
  public function getValue() {
    return $this->value;
  }

}
