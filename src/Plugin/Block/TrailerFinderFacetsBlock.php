<?php

namespace Drupal\unsm_finder\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Drupal\unsm_finder\Client\UnsinnTrailerDetailsClientInterface;
use Drupal\unsm_finder\Client\UnsinnTrailerFinderClientInterface;
use Drupal\unsm_finder\Constants;
use Drupal\unsm_finder\FinderFacetsMetadata;
use Drupal\unsm_finder\FinderFilter;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Provides the trailer finder facets block (consumed via Webservice).
 *
 * @Block(
 *   id = "trailer_finder_facets_block",
 *   admin_label = @Translation("Trailer finder facets"),
 * )
 */
class TrailerFinderFacetsBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $currentRoute;

  /**
   * The UNSINN trailer details client.
   *
   * @var \Drupal\unsm_finder\Client\UnsinnTrailerDetailsClientInterface
   */
  protected $detailsClient;

  /**
   * The UNSINN trailer finder client.
   *
   * @var \Drupal\unsm_finder\Client\UnsinnTrailerFinderClientInterface
   */
  protected $finderClient;

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * Constructs a new TrailerFinderFacetsBlock object.
   *
   * @param array $configuration
   *   The plugin configuration, i.e. an array with configuration values keyed
   *   by configuration option name. The special key 'context' may be used to
   *   initialize the defined contexts by setting it to an array of context
   *   values keyed by context names.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\unsm_finder\Client\UnsinnTrailerDetailsClientInterface $details_client
   *   The UNSINN trailer finder client.
   * @param \Drupal\unsm_finder\Client\UnsinnTrailerFinderClientInterface $finder_client
   *   The UNSINN trailer finder client.
   * @param \Drupal\Core\Routing\RouteMatchInterface $current_route
   *   The route match.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, UnsinnTrailerDetailsClientInterface $details_client, UnsinnTrailerFinderClientInterface $finder_client, RouteMatchInterface $current_route, RequestStack $request_stack) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->detailsClient = $details_client;
    $this->finderClient = $finder_client;
    $this->currentRoute = $current_route;
    $this->request = $request_stack->getCurrentRequest();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('unsm_finder.api_client.unsinn_trailer_details'),
      $container->get('unsm_finder.api_client.unsinn_trailer_finder'),
      $container->get('current_route_match'),
      $container->get('request_stack')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $block = [];
    if (!in_array($this->currentRoute->getRouteName(), ['unsm_finder.finder.result', 'unsm_finder.trailer.details'])) {
      // Do not show the facets on any other page than the finder.
      return $block;
    }

    $block['reset'] = [
      '#type' => 'link',
      '#title' => 'Suche zurücksetzen',
      '#url' => Url::fromRoute('unsm_finder.finder.result'),
      '#attributes' => [
        'class' => ['button', 'button--reset', 'small', 'expanded'],
      ],
    ];

    $filter_raw = $this->request->query->all(Constants::FILTER_URL_PARAM);
    $filter = FinderFilter::fromArray($filter_raw);
    if ($this->currentRoute->getRouteName() == 'unsm_finder.trailer.details') {
      $trailer_id = $this->currentRoute->getParameter('trailer_id');
      if ($trailer_id) {
        if ($trailer = $this->detailsClient->getTrailerDetails($trailer_id)) {
          if ($model_id = $trailer->getModelId()) {
            $filter->setModels([$model_id]);
          }
          if ($trailer_type_candidates = $trailer->getTrailerTypes()) {
            $trailer_types = [];
            $filter_trailer_types = $filter->getTrailerTypes();
            if (!empty($filter_trailer_types)) {
              foreach ($filter_trailer_types as $filter_trailer_type) {
                if (in_array($filter_trailer_type, $trailer_type_candidates)) {
                  $trailer_types[] = $filter_trailer_type;
                  break;
                }
              }
            }
            if (empty($trailer_types)) {
              $trailer_types[] = reset($trailer_type_candidates);
            }
            $filter->setTrailerTypes($trailer_types);
          }
        }
      }
    }
    $facets = $this->finderClient->buildFacets($filter);
    foreach ($facets as $facet) {
      $facet_id = $facet->getId();
      $facet_key = 'facet_' . $facet_id;
      $block[$facet_key] = [
        '#theme' => 'finder_facet',
        '#facet_id' => $facet_id,
        '#label' => $facet->getLabel(),
        '#items' => $facet->getItems(),
        '#widget_type' => FinderFacetsMetadata::getWidgetType($facet_id),
        '#parent_widget' => FinderFacetsMetadata::getParentWidget($facet_id),
        '#weight' => FinderFacetsMetadata::getWeight($facet_id),
        '#postfix' => FinderFacetsMetadata::getPostfix($facet_id),
      ];
      if ($block[$facet_key]['#widget_type'] == 'select') {
        $block[$facet_key]['#attached']['drupalSettings']['facets']['select2_dropdown_widget'][$facet_id]['facet-default-option-label'] = $facet->getLabel() . ' auswählen';
        $block[$facet_key]['#attached']['library'][] = 'unsm_finder/facets.select2-dropdown-widget';
      }
      elseif ($block[$facet_key]['#widget_type'] == 'checkboxes') {
        $block[$facet_key]['#attached']['library'][] = 'unsm_finder/facets.checkboxes-widget';
      }
      elseif ($block[$facet_key]['#widget_type'] == 'range_slider') {
        $items = $facet->getItems();
        if (!empty($items)) {
          $first_item = reset($items);
          $values = [];
          foreach ($items as $item) {
            $values[] = $item->getValue();
          }
          $block[$facet_key]['#attached']['drupalSettings']['facets']['exact_sliders'][$facet_id] = [
            'values' => $values,
            'url' => $first_item->getUrl()->toString(),
          ];
          $block[$facet_key]['#attached']['library'][] = 'unsm_finder/facets.slider-exact-widget';
        }
      }
      $is_active = FALSE;
      foreach ($facet->getItems() as $item) {
        if ($item->isActive()) {
          $is_active = TRUE;
          break;
        }
      }
      $block[$facet_key]['#is_active'] = $is_active;
    }
    foreach ($block as $facet_id => &$facet_renderable) {
      if (!empty($facet_renderable['#parent_widget'])) {
        $parent_id = $facet_renderable['#parent_widget'];
        $parent_id = 'facet_' . $parent_id;
        if (empty($block[$parent_id]['#is_active'])) {
          // Hide dependent facet, when the parent is not active.
          $facet_renderable['#access'] = FALSE;
        }
      }
    }
    return $block;
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    if (!empty($this->configuration['label'])) {
      return $this->configuration['label'];
    }

    return $this->t('Trailer finder');
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    // Never cache a facets block.
    return 0;
  }

}
