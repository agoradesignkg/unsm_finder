<?php

namespace Drupal\unsm_finder\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\unsm_finder\Client\UnsinnTrailerDetailsClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a block to display the page title on remote trailer pages.
 *
 * @Block(
 *   id = "remote_trailer_page_title_block",
 *   admin_label = @Translation("Remote trailer page title"),
 * )
 */
class RemoteTrailerPageTitleBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $currentRoute;

  /**
   * The UNSINN trailer details client.
   *
   * @var \Drupal\unsm_finder\Client\UnsinnTrailerDetailsClientInterface
   */
  protected $detailsClient;

  /**
   * Constructs a new RemoteTrailerPageTitleBlock object.
   *
   * @param array $configuration
   *   The plugin configuration, i.e. an array with configuration values keyed
   *   by configuration option name. The special key 'context' may be used to
   *   initialize the defined contexts by setting it to an array of context
   *   values keyed by context names.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Routing\RouteMatchInterface $current_route
   *   The route match.
   * @param \Drupal\unsm_finder\Client\UnsinnTrailerDetailsClientInterface $details_client
   *   The UNSINN trailer finder client.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, RouteMatchInterface $current_route, UnsinnTrailerDetailsClientInterface $details_client) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->currentRoute = $current_route;
    $this->detailsClient = $details_client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_route_match'),
      $container->get('unsm_finder.api_client.unsinn_trailer_details')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $block = [];
    if ($this->currentRoute->getRouteName() != 'unsm_finder.trailer.details') {
      // Do not show the facets on any other page than the finder.
      return $block;
    }

    $trailer_id = $this->currentRoute->getParameter('trailer_id');
    if (!$trailer_id) {
      return $block;
    }

    $trailer = $this->detailsClient->getTrailerDetails($trailer_id);
    if (!$trailer) {
      return $block;
    }

    $block = [
      '#theme' => 'remote_trailer_page_title',
      '#trailer' => $trailer,
    ];

    return $block;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return Cache::mergeContexts(parent::getCacheContexts(), ['route']);
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    // Cache result for 1 day.
    $max_age = 86400;
    return Cache::mergeMaxAges($max_age, parent::getCacheMaxAge());
  }

}
