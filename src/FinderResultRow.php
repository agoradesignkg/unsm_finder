<?php

namespace Drupal\unsm_finder;

use Drupal\Core\Url;

/**
 * Value object representing a single finder search result row.
 */
final class FinderResultRow {

  /**
   * The trailer ID.
   *
   * @var int
   */
  protected $id;

  /**
   * The trailer title.
   *
   * @var string
   */
  protected $title;

  /**
   * The trailer model.
   *
   * @var string
   */
  protected $model;

  /**
   * The trailer image url.
   *
   * @var \Drupal\Core\Url|null
   */
  protected $imageUrl;

  /**
   * The total weight (formatted).
   *
   * @var string
   */
  protected $totalWeight;

  /**
   * The internal dimensions (formatted).
   *
   * @var string
   */
  protected $internalDimensions;

  /**
   * The remote trailer url.
   *
   * @var \Drupal\Core\Url|null
   */
  protected $remoteUrl;

  /**
   * Factory method, instantiating a new FinderResultRow object from array.
   *
   * The array structure is based on the expectations of the structure returned
   * by UNSINN trailer finder REST web service.
   *
   * @param array $values
   *   The values of a single result row.
   * @param string $base_url
   *   An optional base URL for the image and detail urls, if they are provided
   *   with relative urls - they are currently provided with relative urls.
   *
   * @return static
   *   A new FinderResultRow object, instantiated based on the given values.
   */
  public static function fromArray(array $values, $base_url = '') {
    $result = new static();
    $result->id = $values['trailer_id'];
    $result->title = $values['title'];
    $result->model = $values['model'];
    if (!empty($values['image'])) {
      $url = $values['image'];
      if ($base_url) {
        $url = $base_url . $url;
      }
      $result->imageUrl = Url::fromUri($url);
    }
    if (!empty($values['url'])) {
      $url = $values['url'];
      if ($base_url) {
        $url = $base_url . $url;
      }
      $result->remoteUrl = Url::fromUri($url);
    }
    $result->totalWeight = $values['total_weight'];
    $result->internalDimensions = $values['internal_dimensions'];
    return $result;
  }

  /**
   * Returns an array representation of this object.
   *
   * Main purpose is to have an array to access to in our Twig template. It is
   * not a render array per se, but all components are either strings, render
   * arrays or at least compatible to be renderable.
   *
   * @return array
   *   An array representation of this object.
   */
  public function toArray() {
    return [
      'id' => $this->id,
      'title' => $this->title,
      'model' => $this->model,
      'image' => $this->imageUrl,
      'total_weight' => $this->totalWeight,
      'internal_dimensions' => ['#markup' => $this->internalDimensions],
      'remote_url' => $this->remoteUrl,
    ];
  }

  /**
   * @return int
   */
  public function getId() {
    return $this->id;
  }

}
