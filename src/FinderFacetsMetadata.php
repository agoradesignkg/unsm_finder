<?php

namespace Drupal\unsm_finder;

/**
 * Provides some metadata (hardcoded mappings) for facets.
 */
class FinderFacetsMetadata {

  /**
   * Facets metadata storage.
   *
   * @var array
   */
  protected static $facets = [
    'axles' => [
      'label' => 'Achsen',
      'weight' => 5,
      'widget' => 'checkboxes',
      'parent_widget' => NULL,
      'postfix' => '',
    ],
    'braked' => [
      'label' => 'Gebremst',
      'weight' => 6,
      'widget' => 'checkboxes',
      'parent_widget' => NULL,
      'postfix' => '',
    ],
    'category' => [
      'label' => 'Anhängertyp',
      'weight' => 1,
      'widget' => 'select',
      'parent_widget' => 'field_trailer_types',
      'postfix' => '',
    ],
    'field_attr_accessories' => [
      'label' => 'Eigenschaften / Zubehör',
      'weight' => 7,
      'widget' => 'checkboxes',
      'parent_widget' => NULL,
      'postfix' => '',
    ],
    'field_trailer_types' => [
      'label' => 'Kategorie',
      'weight' => 0,
      'widget' => 'select',
      'parent_widget' => NULL,
      'postfix' => '',
    ],
    'internal_dimensions_length' => [
      'label' => 'Innenlänge',
      'weight' => 2,
      'widget' => 'range_slider',
      'parent_widget' => NULL,
      'postfix' => 'mm',
    ],
    'internal_dimensions_width' => [
      'label' => 'Innenbreite',
      'weight' => 3,
      'widget' => 'range_slider',
      'parent_widget' => NULL,
      'postfix' => 'mm',
    ],
    'total_weight' => [
      'label' => 'Gesamtgewicht',
      'weight' => 4,
      'widget' => 'range_slider',
      'parent_widget' => NULL,
      'postfix' => 'kg',
    ],
  ];

  /**
   * Returns the appropriate label for the given facet ID.
   *
   * @param string $facet_id
   *   The facet ID.
   *
   * @return string
   *   The appropriate label.
   */
  public static function getLabel($facet_id) {
    return isset(static::$facets[$facet_id]) ? static::$facets[$facet_id]['label'] : $facet_id;
  }

  /**
   * Returns the appropriate sort weight for the given facet ID.
   *
   * @param string $facet_id
   *   The facet ID.
   *
   * @return int
   *   The appropriate sort weight.
   */
  public static function getWeight($facet_id) {
    return isset(static::$facets[$facet_id]) ? static::$facets[$facet_id]['weight'] : 99;
  }

  /**
   * Returns the widget type to use for the given facet ID.
   *
   * @param string $facet_id
   *   The facet ID.
   *
   * @return string
   *   The widget type to use.
   */
  public static function getWidgetType($facet_id) {
    return isset(static::$facets[$facet_id]) ? static::$facets[$facet_id]['widget'] : 'checkboxes';
  }

  /**
   * Returns the parent widget for the given facet ID.
   *
   * This information can be used for dependent select lists for example.
   *
   * @param string $facet_id
   *   The facet ID.
   *
   * @return string|null
   *   The parent widget or NULL, if the facet is not dependent on another one.
   */
  public static function getParentWidget($facet_id) {
    return isset(static::$facets[$facet_id]) ? static::$facets[$facet_id]['parent_widget'] : NULL;
  }

  /**
   * Returns the label postfix for the given facet ID.
   *
   * @param string $facet_id
   *   The facet ID.
   *
   * @return string
   *   The label postfix (only relevant for range sliders).
   */
  public static function getPostfix($facet_id) {
    return isset(static::$facets[$facet_id]) ? static::$facets[$facet_id]['postfix'] : '';
  }

}
