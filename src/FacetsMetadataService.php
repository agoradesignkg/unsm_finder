<?php

namespace Drupal\unsm_finder;

use Drupal\Core\KeyValueStore\KeyValueExpirableFactoryInterface;
use Drupal\unsm_finder\Client\UnsinnTrailerFinderClientInterface;

/**
 * Default facets metadata service implementation.
 */
class FacetsMetadataService implements FacetsMetadataServiceInterface {

  /**
   * The UNSINN trailer finder client.
   *
   * @var \Drupal\unsm_finder\Client\UnsinnTrailerFinderClientInterface
   */
  protected $finderClient;

  /**
   * The key/value storage object used for this data.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueStoreExpirableInterface
   */
  protected $storage;

  /**
   * Static cache of whether the metadata have been updated during this request.
   *
   * We hold this information because under certain circumstances of missing
   * data we will trigger an update during the request. But this should only
   * happen once of course.
   *
   * @var bool
   */
  protected $updated;

  /**
   * Constructs a new FacetsMetadataService object.
   *
   * @param \Drupal\unsm_finder\Client\UnsinnTrailerFinderClientInterface $finder_client
   *   The UNSINN trailer finder client.
   * @param \Drupal\Core\KeyValueStore\KeyValueExpirableFactoryInterface $key_value_expirable_factory
   *   The key/value storage factory, that creates a storage object used for
   *   this data. Each storage object represents a particular collection of data
   *   and will contain any number of key/value pairs.
   */
  public function __construct(UnsinnTrailerFinderClientInterface $finder_client, KeyValueExpirableFactoryInterface $key_value_expirable_factory) {
    $this->finderClient = $finder_client;
    $this->storage = $key_value_expirable_factory->get('unsm_finder');
    $this->updated = FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function updateMetadata() {
    if ($this->updated) {
      return FALSE;
    }
    $metadata = $this->finderClient->fetchFacetsMetadata();
    // Set the static cache that we have at least tried to fetch the metadata
    // once in this request.
    $this->updated = TRUE;
    if (empty($metadata)) {
      return FALSE;
    }
    $this->storage->setWithExpire('finder_metadata', $metadata, 3600 * 24);
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getCategoryById($id) {
    $type = 'categories';
    $info = $this->getMetadata($type);
    if (!isset($info[$id])) {
      // Update metadata and try again.
      $this->updateMetadata();
      $info = $this->getMetadata($type);
    }
    return isset($info[$id]) ? $info[$id] : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getTrailerTypeById($id) {
    $type = 'trailer_types';
    $info = $this->getMetadata($type);
    if (!isset($info[$id])) {
      // Update metadata and try again.
      $this->updateMetadata();
      $info = $this->getMetadata($type);
    }
    return isset($info[$id]) ? $info[$id] : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getAccessoryById($id) {
    $type = 'accessories';
    $info = $this->getMetadata($type);
    if (!isset($info[$id])) {
      // Update metadata and try again.
      $this->updateMetadata();
      $info = $this->getMetadata($type);
    }
    return isset($info[$id]) ? $info[$id] : NULL;
  }

  /**
   * Get the metadata for the given type.
   *
   * @param string $type
   *   The metadata type. One of 'accessories', 'categories', 'trailer_types'.
   *
   * @return array
   *   An array of metadata for the given facet type. At least the 'label' key
   *   must exist. Taxonomy terms may also have set a 'thumbnail' key.
   */
  protected function getMetadata($type) {
    $metadata = $this->storage->get('finder_metadata');
    if (empty($metadata)) {
      $this->updateMetadata();
      $metadata = $this->storage->get('finder_metadata');
      if (empty($metadata)) {
        return [];
      }
    }
    return isset($metadata[$type]) ? $metadata[$type] : [];
  }

}
