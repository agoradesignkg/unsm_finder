<?php

namespace Drupal\unsm_finder;

use Drupal\unsm_finder\Form\Value\AccessoryQuantitySelection;
use Drupal\unsm_finder\Trailer\RemoteTrailer;

/**
 * Provides an interface for temporary storing selected accessories per trailer.
 */
interface RemoteTrailerAccessorySelectionStoreInterface {

  /**
   * Stores the given trailer with the selected accessories in the session.
   *
   * @param \Drupal\unsm_finder\Trailer\RemoteTrailer $trailer
   *   The trailer entity.
   *
   * @param \Drupal\unsm_finder\Form\Value\AccessoryQuantitySelection[] $accessories
   *   The selected accessories.
   *
   * @return void
   */
  public function selectAccessories(RemoteTrailer $trailer, array $accessories);

  /**
   * Updates the quantity for the given trailer and accessory selection item.
   *
   * @param \Drupal\unsm_finder\Trailer\RemoteTrailer $trailer
   *   The trailer entity.
   * @param \Drupal\unsm_finder\Form\Value\AccessoryQuantitySelection $accessory
   *   The selected accessory.
   * @param bool $strict
   *   Whether or not to act in strict mode. Strict mode means that the quantity
   *   is only updated, if the item was already found in the assigned accessory
   *   list. If item is not in list, but strict mode is disabled, the item gets
   *   added to the list.
   *
   * @return void
   */
  public function updateQuantity(RemoteTrailer $trailer, AccessoryQuantitySelection $accessory, $strict = TRUE);

  /**
   * Loads the selected accessories for the given trailer from the session.
   *
   * @param \Drupal\unsm_finder\Trailer\RemoteTrailer $trailer
   *   The trailer entity.
   *
   * @return \Drupal\unsm_finder\Form\Value\AccessoryQuantitySelection[]
   *   The selected accessories.
   */
  public function loadSelectedAccessories(RemoteTrailer $trailer);

  /**
   * Loads the raw values of selected accessories for the given trailer.
   *
   * @param \Drupal\unsm_finder\Trailer\RemoteTrailer $trailer
   *   The trailer entity.
   *
   * @return array
   *   The selected accessories.
   */
  public function loadSelectedAccessoriesRaw(RemoteTrailer $trailer);

}
