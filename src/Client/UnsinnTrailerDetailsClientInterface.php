<?php

namespace Drupal\unsm_finder\Client;

/**
 * Defines the UNSINN trailer details client interface.
 */
interface UnsinnTrailerDetailsClientInterface {

  /**
   * Gets trailer details for the given ID.
   *
   * @param int $trailer_id
   *   UNSINN trailer ID.
   *
   * @return \Drupal\unsm_finder\Trailer\RemoteTrailer|null
   */
  public function getTrailerDetails($trailer_id);

}
