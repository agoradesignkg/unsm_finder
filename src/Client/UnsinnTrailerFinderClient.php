<?php

namespace Drupal\unsm_finder\Client;

use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Url;
use Drupal\unsm_finder\Constants;
use Drupal\unsm_finder\FinderFacet;
use Drupal\unsm_finder\FinderFilter;
use Drupal\unsm_finder\FinderResult;
use GuzzleHttp\ClientInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Serializer\Encoder\DecoderInterface;

/**
 * Default UNSINN trailer finder client implementation.
 */
class UnsinnTrailerFinderClient implements UnsinnTrailerFinderClientInterface {

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The trailer finder endpoint url.
   *
   * @var string
   */
  protected $endpoint;

  /**
   * The trailer finder metadata endpoint url.
   *
   * @var string
   */
  protected $metadataEndpoint;

  /**
   * The trailer finder image base url.
   *
   * @var string
   */
  protected $imageBaseUrl;

  /**
   * The trailer finder url key.
   *
   * @var string
   */
  protected $urlKey;

  /**
   * Static cache. Handy for having access from multiple places to the result.
   *
   * Without the need of querying over and over again.
   *
   * @var array
   */
  protected $responseCache;

  /**
   * The serializer.
   *
   * @var \Symfony\Component\Serializer\Encoder\DecoderInterface
   */
  protected $serializer;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Constructs a new UnsinnTrailerFinderClient object.
   *
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The HTTP client.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Symfony\Component\Serializer\Encoder\DecoderInterface $serializer
   *   The serializer.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.
   */
  public function __construct(ClientInterface $http_client, ConfigFactoryInterface $config_factory, DecoderInterface $serializer, LoggerInterface $logger) {
    $this->httpClient = $http_client;
    $this->serializer = $serializer;
    $this->logger = $logger;
    $config = $config_factory->get('unsm_finder.settings');
    $this->endpoint = $config->get('endpoint');
    $this->metadataEndpoint = $config->get('metadata_endpoint');
    $this->imageBaseUrl = $config->get('image_base_url');
    $this->urlKey = $config->get('url_key');
    $this->responseCache = [];
  }

  /**
   * {@inheritdoc}
   */
  public function query(FinderFilter $filter = NULL) {
    $response_body = $this->doQuery($filter);
    return FinderResult::fromArray($response_body, $this->imageBaseUrl);
  }

  /**
   * {@inheritdoc}
   */
  public function buildFacets(FinderFilter $filter = NULL) {
    $result = [];
    $response_body = $this->doQuery($filter);
    if (!empty($response_body['facets'])) {
      foreach ($response_body['facets'] as $row) {
        foreach ($row as $inner_row) {
          if (!empty(Element::children($inner_row))) {
            foreach ($inner_row as $facet_id => $facet_items) {
              if (!is_array($facet_items)) {
                continue;
              }
              foreach ($facet_items as &$facet_item) {
                if (!is_array($facet_item)) {
                  continue;
                }
                if (!empty($facet_item['url'])) {
                  $parsed_url = UrlHelper::parse($facet_item['url']);
                  $args = !empty($parsed_url['query'][$this->urlKey]) ? $parsed_url['query'][$this->urlKey] : [];
                  $facet_item['url'] = Url::fromRoute('unsm_finder.finder.result', [], ['query' => [Constants::FILTER_URL_PARAM => $args]]);
                }
              }
              $result[$facet_id] = FinderFacet::fromArray($facet_id, $facet_items);
            }
          }
        }
      }
    }
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function fetchFacetsMetadata() {
    $target_url = Url::fromUri($this->metadataEndpoint);
    /** @var \GuzzleHttp\Psr7\Response $response */
    $response = $this->httpClient->get($target_url->toString(), ['Accept' => 'application/json']);
    if ($response->getStatusCode() == 200) {
      $response_body = $this->serializer->decode($response->getBody(), 'json');
      if (empty($response_body) || !is_array($response_body)) {
        $this->logger->error('Received invalid response from facets metadata webservice. Response is not valid JSON.');
        return [];
      }
      return $response_body;
    }
    $this->logger->error('Received invalid response from facets metadata webservice. Status code: @status', ['@status' => $response->getStatusCode()]);
    return [];
  }

  /**
   * Queries the trailer finder web service, using static response cache.
   *
   * @param \Drupal\unsm_finder\FinderFilter|NULL $filter
   *   The filter values as value object. Or NULL, if no filter should be used.
   *
   * @return array
   *   The received response as JSON array.
   */
  protected function doQuery(FinderFilter $filter = NULL) {
    $cache_key = $filter ? md5(Json::encode($filter->toArray(TRUE))) : 0;
    if (isset($this->responseCache[$cache_key])) {
      // Return the cached response.
      return $this->responseCache[$cache_key];
    }

    $target_url = Url::fromUri($this->endpoint);
    if ($filter) {
      $query_params = [$this->urlKey => $filter->toArray()];
      if ($filter->getPage()) {
        $query_params['page'] = $filter->getPage();
      }
      $target_url->setOption('query', $query_params);
    }
    /** @var \GuzzleHttp\Psr7\Response $response */
    $response = $this->httpClient->get($target_url->toString(), ['Accept' => 'application/json']);

    $response_body = $this->serializer->decode($response->getBody(), 'json');
    if (empty($response_body) || !is_array($response_body)) {
      // @todo is an empty array the way to go for invalid responses?
      $this->responseCache[$cache_key] = [];
      return [];
    }
    $this->responseCache[$cache_key] = $response_body;
    return $response_body;
  }

}
