<?php

namespace Drupal\unsm_finder\Client;

use Drupal\unsm_finder\FinderFilter;

/**
 * Defines the UNSINN trailer finder client interface.
 */
interface UnsinnTrailerFinderClientInterface {

  /**
   * Query the trailer finder, given an optional filter (facet).
   *
   * @param \Drupal\unsm_finder\FinderFilter $filter
   *   The filter values as value object. Or NULL, if no filter should be used.
   *
   * @return \Drupal\unsm_finder\FinderResult
   *   A FinderResult object.
   */
  public function query(FinderFilter $filter = NULL);

  /**
   * Build the trailer facets, given an optional filter.
   *
   * @param \Drupal\unsm_finder\FinderFilter $filter
   *   The filter values as value object. Or NULL, if no filter should be used.
   *
   * @return \Drupal\unsm_finder\FinderFacet[]
   *   An array of FinderFacet objects.
   */
  public function buildFacets(FinderFilter $filter = NULL);

  /**
   * Fetches the facets metadata (labels).
   *
   * @return array
   *   The webservice response.
   */
  public function fetchFacetsMetadata();

}
