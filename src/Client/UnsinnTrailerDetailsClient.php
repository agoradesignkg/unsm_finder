<?php

namespace Drupal\unsm_finder\Client;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Url;
use Drupal\unsm_finder\Trailer\RemoteTrailer;
use GuzzleHttp\ClientInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Serializer\Encoder\DecoderInterface;

/**
 * Default UNSINN trailer details client implementation.
 */
class UnsinnTrailerDetailsClient implements UnsinnTrailerDetailsClientInterface {

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The trailer finder endpoint url.
   *
   * @var string
   */
  protected $endpoint;

  /**
   * Static cache. Handy for having access from multiple places to the result.
   *
   * Without the need of querying over and over again.
   *
   * @var array
   */
  protected $responseCache;

  /**
   * The serializer.
   *
   * @var \Symfony\Component\Serializer\Encoder\DecoderInterface
   */
  protected $serializer;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Constructs a new UnsinnTrailerDetailsClient object.
   *
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The HTTP client.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Symfony\Component\Serializer\Encoder\DecoderInterface $serializer
   *   The serializer.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.
   */
  public function __construct(ClientInterface $http_client, ConfigFactoryInterface $config_factory, DecoderInterface $serializer, LoggerInterface $logger) {
    $this->httpClient = $http_client;
    $this->serializer = $serializer;
    $this->logger = $logger;
    $config = $config_factory->get('unsm_finder.settings');
    $this->endpoint = $config->get('details_endpoint');
    $this->responseCache = [];
  }

  /**
   * {@inheritdoc}
   */
  public function getTrailerDetails($trailer_id) {
    $response_body = $this->queryTrailerDetails($trailer_id);
    if (empty($response_body)) {
      return NULL;
    }
    return RemoteTrailer::fromArray($response_body);
  }

  /**
   * Queries the trailer details web service, using static response cache.
   *
   * @param int $trailer_id
   *   The UNSINN trailer ID.
   *
   * @return array
   *   The received response as JSON array.
   */
  protected function queryTrailerDetails($trailer_id) {
    $cache_key = $trailer_id;
    if (isset($this->responseCache[$cache_key])) {
      // Return the cached response.
      return $this->responseCache[$cache_key];
    }

    $target_url = Url::fromUri(sprintf('%s/%s', $this->endpoint, $trailer_id));
    /** @var \GuzzleHttp\Psr7\Response $response */
    $response = $this->httpClient->get($target_url->toString(), ['Accept' => 'application/json']);

    $response_body = $this->serializer->decode($response->getBody(), 'json');
    if (empty($response_body) || !is_array($response_body)) {
      // @todo is an empty array the way to go for invalid responses?
      $this->responseCache[$cache_key] = [];
      return [];
    }
    $this->responseCache[$cache_key] = $response_body;
    return $response_body;
  }

}
