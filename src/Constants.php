<?php

namespace Drupal\unsm_finder;

/**
 * Defines some commonly needed constants.
 *
 * These constants do not really fit into any of the involved classes or
 * interfaces, to be referenced by other use cases.
 */
final class Constants {

  /**
   * The query string parameter name used for the filter.
   */
  const FILTER_URL_PARAM = 'filter';

}
