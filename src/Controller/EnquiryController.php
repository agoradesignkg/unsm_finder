<?php

namespace Drupal\unsm_finder\Controller;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Routing\LocalRedirectResponse;
use Drupal\Core\Url;
use Drupal\unsm_finder\Client\UnsinnTrailerDetailsClientInterface;
use Drupal\unsm_finder\RemoteTrailerAccessorySelectionStoreInterface;
use Drupal\unsm_finder\Trailer\RemoteTrailer;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Defines the enquiry controller.
 */
class EnquiryController extends ControllerBase {

  /**
   * The UNSINN trailer details client.
   *
   * @var \Drupal\unsm_finder\Client\UnsinnTrailerFinderClientInterface
   */
  protected $client;

  /**
   * Static cache for remote trailers.
   *
   * @var \Drupal\unsm_finder\Trailer\RemoteTrailer[]
   */
  protected $trailerCache;

  /**
   * The trailer accessory selection store.
   *
   * @var \Drupal\unsm_finder\RemoteTrailerAccessorySelectionStoreInterface
   */
  protected $trailerAccessorySelectionStore;

  /**
   * Constructs a new TrailerDetailsController object.
   *
   * @param \Drupal\unsm_finder\Client\UnsinnTrailerDetailsClientInterface $client
   *   The UNSINN trailer finder client.
   * @param \Drupal\unsm_finder\RemoteTrailerAccessorySelectionStoreInterface $trailer_accessory_selection_store
   *   The trailer accessory selection store.
   */
  public function __construct(UnsinnTrailerDetailsClientInterface $client, RemoteTrailerAccessorySelectionStoreInterface $trailer_accessory_selection_store) {
    $this->client = $client;
    $this->trailerAccessorySelectionStore = $trailer_accessory_selection_store;
    $this->trailerCache = [];
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('unsm_finder.api_client.unsinn_trailer_details'),
      $container->get('unsm_finder.remote_trailer_accessory_selection_store')
    );
  }

  /**
   * Shows the enquiry form for the given trailer.
   *
   * @param int $trailer_id
   *   The trailer ID.
   *
   * @return array
   *   The response as render array.
   */
  public function form($trailer_id) {
    if (empty($trailer_id) || !is_numeric($trailer_id) || $trailer_id < 1) {
      throw new NotFoundHttpException();
    }

    if (!isset($this->trailerCache[$trailer_id])) {
      $trailer_details = $this->client->getTrailerDetails($trailer_id);
      if (empty($trailer_details)) {
        throw new NotFoundHttpException();
      }
      $this->trailerCache[$trailer_id] = $trailer_details;
    }
    $remote_trailer = $this->trailerCache[$trailer_id];

    $page = [
      '#theme' => 'enquiry_page',
    ];
    $page['#trailer_pre_header'] = [
      '#type' => 'html_tag',
      '#tag' => 'h2',
      '#value' => 'Ihr gewählter Anhänger',
      '#weight' => 0,
    ];

    $page['#trailer'] = [
      '#theme' => 'remote_trailer_summary',
      '#trailer' => $remote_trailer,
      '#cache' => [
        // Cache result for 1 day.
        'max-age' => 86400,
      ],
      '#weight' => 5,
    ];

    $is_new_trailer = $remote_trailer->getBundle() == 'default';
    // This condition is also checked on UNSINN page. The question is, whether
    // we should just rely on web service data instead?
    if ($is_new_trailer) {
      $accessory_table_form = $this->formBuilder()->getForm('Drupal\unsm_finder\Form\RemoteTrailerPreselectedAccessoryTableForm', $remote_trailer);
      $page['#accessories_pre_header'] = [
        '#type' => 'html_tag',
        '#tag' => 'h2',
        '#value' => 'Ihr gewähltes Zubehör',
        '#weight' => 10,
      ];
      $page['#accessories'] = $accessory_table_form;
      $page['#accessories']['#weight'] = 15;
    }

    $page['#enquiry'] = [
      '#type' => 'webform',
      '#webform' => 'enquiry',
      '#default_data' => [
        'trailer' => $remote_trailer->getId(),
        'accessories' => serialize($this->trailerAccessorySelectionStore->loadSelectedAccessoriesRaw($remote_trailer)),
      ],
      '#weight' => 20,
    ];

    return $page;
  }

  /**
   * Removes an accessory from the selected parts in the enquiry form.
   *
   * @param int $trailer_id
   *   The trailer ID.
   * @param int $accessory_id
   *   The accessory ID.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   The redirect response to the enquiry form.
   */
  public function removeAccessory($trailer_id, $accessory_id) {
    // Do not load the full trailer information from remote server, as we only
    // need the ID, but our API is defined to pass around the value objects.
    $trailer_dummy = new RemoteTrailer($trailer_id);

    $selected_accessories = $this->trailerAccessorySelectionStore->loadSelectedAccessories($trailer_dummy);
    $changed = FALSE;
    foreach ($selected_accessories as $idx => $selected_accessory) {
      if ((int)$selected_accessory->getSparepartId() === (int)$accessory_id) {
        unset($selected_accessories[$idx]);
        $changed = TRUE;
        break;
      }
    }
    if ($changed) {
      $this->trailerAccessorySelectionStore->selectAccessories($trailer_dummy, $selected_accessories);
      $this->messenger()->addStatus('Das Zubehörteil wurde aus Ihrer Auswahl entfernt.');
    }
    else {
      // @todo Sollen wir hier eine Warnmeldung anzeigen? Eigentlich brauch ma des ned, oder?
    }
    $enquiry_url = Url::fromRoute('unsm_finder.enquiry.form', ['trailer_id' => $trailer_id], ['absolute' => TRUE]);
    $response = new LocalRedirectResponse($enquiry_url->toString(TRUE)->getGeneratedUrl());
    $cacheable_metadata = new CacheableMetadata();
    $cacheable_metadata->setCacheMaxAge(0);
    $response->addCacheableDependency($cacheable_metadata);
    return $response;
  }

}
