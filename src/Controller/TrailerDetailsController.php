<?php

namespace Drupal\unsm_finder\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\unsm_finder\Client\UnsinnTrailerDetailsClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Defines the remote trailer details controller.
 */
class TrailerDetailsController extends ControllerBase {

  /**
   * The UNSINN trailer details client.
   *
   * @var \Drupal\unsm_finder\Client\UnsinnTrailerFinderClientInterface
   */
  protected $client;

  /**
   * Static cache for remote trailers.
   *
   * @var \Drupal\unsm_finder\Trailer\RemoteTrailer[]
   */
  protected $trailerCache;

  /**
   * Constructs a new TrailerDetailsController object.
   *
   * @param \Drupal\unsm_finder\Client\UnsinnTrailerDetailsClientInterface $client
   *   The UNSINN trailer finder client.
   */
  public function __construct(UnsinnTrailerDetailsClientInterface $client) {
    $this->client = $client;
    $this->trailerCache = [];
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('unsm_finder.api_client.unsinn_trailer_details')
    );
  }

  /**
   * Renders the remote trailer details page.
   *
   * @param int $trailer_id
   *   The trailer ID.
   *
   * @return array
   *   The page as render array.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
   *   If an invalid trailer ID is defined.
   */
  public function page($trailer_id) {
    if (empty($trailer_id) || !is_numeric($trailer_id) || $trailer_id < 1) {
      throw new NotFoundHttpException();
    }
    $page = [];

    if (!isset($this->trailerCache[$trailer_id])) {
      $trailer_details = $this->client->getTrailerDetails($trailer_id);
      if (empty($trailer_details)) {
        throw new NotFoundHttpException();
      }
      $this->trailerCache[$trailer_id] = $trailer_details;
    }
    $remote_trailer = $this->trailerCache[$trailer_id];

    $page['trailer'] = [
      '#theme' => 'remote_trailer',
      '#trailer' => $remote_trailer,
      '#cache' => [
        // Cache result for 1 day.
        'max-age' => 86400,
      ],
      '#weight' => 0,
    ];

    if ($remote_trailer->hasAccessories()) {
      $page['accessories'] = [
        '#type' => 'container',
        '#attributes' => [
          'class' => 'trailer__accessories',
        ],
        '#weight' => 1,
      ];

      $page['accessories']['header'] = [
        '#type' => 'html_tag',
        '#tag' => 'h2',
        '#value' => 'Wählen Sie Ihr Zubehör',
      ];
      $page['accessories']['form'] = $this->formBuilder()->getForm('\Drupal\unsm_finder\Form\RemoteTrailerAccessoryTableForm', $remote_trailer);
    }

    return $page;
  }

  /**
   * Title callback for the trailer finder results page.
   *
   * @return string|\Drupal\Core\StringTranslation\TranslatableMarkup
   *   The page title for the trailer finder results page.
   */
  public function pageTitle($trailer_id) {
    if (!empty($trailer_id) && is_numeric($trailer_id) && $trailer_id > 0) {
      if (!isset($this->trailerCache[$trailer_id])) {
        $trailer_details = $this->client->getTrailerDetails($trailer_id);
        if (!empty($trailer_details)) {
          $this->trailerCache[$trailer_id] = $trailer_details;
        }
      }
      else {
        $trailer_details = $this->trailerCache[$trailer_id];
      }
    }
    return !empty($trailer_details) ? $trailer_details->getFullName() : 'Neuanhänger';
  }

}
