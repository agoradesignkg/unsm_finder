<?php

namespace Drupal\unsm_finder\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Pager\PagerManagerInterface;
use Drupal\Core\Pager\PagerParametersInterface;
use Drupal\unsm_finder\Client\UnsinnTrailerFinderClientInterface;
use Drupal\unsm_finder\Constants;
use Drupal\unsm_finder\FinderFilter;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Defines the trailer finder controller.
 */
class TrailerFinderController extends ControllerBase {

  /**
   * The UNSINN trailer finder client.
   *
   * @var \Drupal\unsm_finder\Client\UnsinnTrailerFinderClientInterface
   */
  protected $finderClient;

  /**
   * The pager manager.
   *
   * @var \Drupal\Core\Pager\PagerManagerInterface
   */
  protected $pagerManager;

  /**
   * The pager parameters.
   *
   * @var \Drupal\Core\Pager\PagerParametersInterface
   */
  protected $pagerParameters;

  /**
   * Constructs a new TrailerFinderController object.
   *
   * @param \Drupal\unsm_finder\Client\UnsinnTrailerFinderClientInterface $finder_client
   *   The UNSINN trailer finder client.
   * @param \Drupal\Core\Pager\PagerManagerInterface $pager_manager
   *   The pager manager.
   * @param \Drupal\Core\Pager\PagerParametersInterface $pager_parameters
   *   The pager parameters.
   */
  public function __construct(UnsinnTrailerFinderClientInterface $finder_client, PagerManagerInterface $pager_manager, PagerParametersInterface $pager_parameters) {
    $this->finderClient = $finder_client;
    $this->pagerManager = $pager_manager;
    $this->pagerParameters = $pager_parameters;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('unsm_finder.api_client.unsinn_trailer_finder'),
      $container->get('pager.manager'),
      $container->get('pager.parameters')
    );
  }

  /**
   * Renders the dedicated trailer finder result page.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   *
   * @return array
   *   The page as render array.
   */
  public function resultPage(Request $request) {
    $filter_raw = $request->query->all(Constants::FILTER_URL_PARAM);
    $filter = FinderFilter::fromArray($filter_raw);

    $page = $this->pagerParameters->findPage();
    $filter->setPage($page);

    $output = [];
    $result = $this->finderClient->query($filter);

    $output['trailer_finder'] = [
      '#theme' => 'finder_result',
      '#result' => $result,
    ];

    if ($result->getPages() > 1) {
      $this->pagerManager->createPager($result->getTotalRows(), $result->getItemsPerPage());
      $output['pagination'] = [
        '#type' => 'pager',
      ];
    }

    return $output;
  }

  /**
   * Title callback for the trailer finder results page.
   *
   * @return string|\Drupal\Core\StringTranslation\TranslatableMarkup
   *   The page title for the trailer finder results page.
   */
  public function pageTitle() {
    return 'Anhängerfinder';
  }

}
