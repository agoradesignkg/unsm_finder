<?php

namespace Drupal\unsm_finder;

/**
 * Defines the facets metadata service interface.
 *
 * Purpose of the facets metadata service is to provide the labels for each of
 * the given facets, as they aren't part of the REST view result. The service is
 * also responsible for caching the results for an appropriate amount of time.
 */
interface FacetsMetadataServiceInterface {

  /**
   * Fetches the metadata from UNSINN server.
   *
   * The call is protected by a static cache variable, so that the request can
   * only be made once per request.
   *
   * @return bool
   *   Whether the update was successful.
   */
  public function updateMetadata();

  /**
   * Get category information by ID.
   *
   * @param int $id
   *   The ID.
   *
   * @return array
   *   The category information. Each entry must have at least the 'label' key
   *   present. Optionally, the 'thumbnail' key may be present - an url to the
   *   thumbnail image.
   */
  public function getCategoryById($id);

  /**
   * Get trailer type information by ID.
   *
   * @param int $id
   *   The ID.
   *
   * @return array
   *   The category information. Each entry must have at least the 'label' key
   *   present. Optionally, the 'thumbnail' key may be present - an url to the
   *   thumbnail image.
   */
  public function getTrailerTypeById($id);

  /**
   * Get accessory information by ID.
   *
   * @param int $id
   *   The ID.
   *
   * @return array
   *   The category information. Each entry must have at least the 'label' key
   *   present.
   */
  public function getAccessoryById($id);

}
