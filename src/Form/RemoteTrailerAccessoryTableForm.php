<?php

namespace Drupal\unsm_finder\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\unsm_finder\Form\Value\AccessoryQuantitySelection;
use Drupal\unsm_finder\RemoteTrailerAccessorySelectionStoreInterface;
use Drupal\unsm_finder\Trailer\RemoteTrailer;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the trailer accessory form for use in a field formatter.
 */
class RemoteTrailerAccessoryTableForm extends FormBase {

  /**
   * The remote trailer accessory selection store.
   *
   * @var \Drupal\unsm_finder\RemoteTrailerAccessorySelectionStoreInterface
   */
  protected $trailerAccessorySelectionStore;

  /**
   * Constructs a new RemoteTrailerAccessoryTableForm object.
   *
   * @param \Drupal\unsm_finder\RemoteTrailerAccessorySelectionStoreInterface $trailer_accessory_selection_store
   *   The trailer accessory selection store.
   */
  public function __construct(RemoteTrailerAccessorySelectionStoreInterface $trailer_accessory_selection_store) {
    $this->trailerAccessorySelectionStore = $trailer_accessory_selection_store;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('unsm_finder.remote_trailer_accessory_selection_store')
    );
  }

  /**
   * Returns a table column mapping from indexes to column names.
   *
   * This is a helper function that can be used in preprocessors to identify
   * column names and e.g. add specific HTML attributes on them.
   * The existence of this workaround is due to the fact, that we found no solid
   * way to integrate both having form fields inside the table and set column
   * attributes. Unless you use the '#rows' property of the table and there
   * inside the renderable style of defining table cells, you don't have to
   * change to set HTML attributes. However, as soon as you start building the
   * render array that way, you lose the ability to properly use form elements.
   *
   * Having this hardcoded static mapping is a good trade off, as we only have
   * to keep this consistent within this class, but gain a reliable way for
   * getting this kind of information from outside. So this is a great way to
   * use inside of template_prepocess_table() implementations.
   */
  public static function getTableColumnMapping() {
    return [
      0 => 'image',
      1 => 'info',
      2 => 'qty',
      3 => 'selection',
    ];
  }

  /**
   * Gets the table column name by its index.
   *
   * See ::getTableColumnMapping() for details about the possible use cases.
   * This function is on top of that to provide a simple and safe way to get the
   * column name of the given column index.

   * @param int $index
   *   The table column index.
   *
   * @return string|null
   *   The column name matching the given index, or NULL if the given value is
   *   unknown or invalid.
   *
   * @see \Drupal\unsm_finder\Form\TrailerAccessoryTableForm::getTableColumnMapping()
   */
  public static function getTableColumnName($index) {
    $mapping = self::getTableColumnMapping();
    return isset($mapping[$index]) ? $mapping[$index] : NULL;
  }

  /**
   * @inheritDoc
   */
  public function getFormId() {
    return 'remote_trailer_accessory_table_form';
  }

  /**
   * @inheritDoc
   */
  public function buildForm(array $form, FormStateInterface $form_state, RemoteTrailer $trailer = NULL) {
    if (empty($trailer)) {
      throw new \InvalidArgumentException('The trailer item passed to trailer_accessory_table_form must not be empty!');
    }

    $accessory_selection_from_session = $this->trailerAccessorySelectionStore->loadSelectedAccessories($trailer);
    $pre_selected_accessories = [];
    // Flatten the structure into a $sparepart_id => $qty array.
    foreach ($accessory_selection_from_session as $selection) {
      $pre_selected_accessories[$selection->getSparepartId()] = $selection->getQty();
    }

    $accessories = [];
    foreach ($trailer->getAccessoryGroups() as $accessory_group) {
      $table = [
        '#type' => 'table',
        '#caption' => $accessory_group->getTitle(),
        '#attributes' => [
          'class' => ['accessory-table'],
          // data-trailer-accessory-table-form is used to identify the table in
          // preprocessors, etc.
          'data-trailer-accessory-table-form' => 1,
        ],
      ];

      foreach ($accessory_group->getAccessories() as $accessory) {
        $row_no = $accessory->getId();

        if ($accessory->getThumbnail()) {
          $table[$row_no]['image'] = [
            '#theme' => 'accessory_image',
            '#thumbnail' => $accessory->getThumbnail() ? $accessory->getThumbnail()->toArray() : [],
            '#zoomed' => $accessory->getImage() ? $accessory->getImage()->toArray() : [],
            '#item_id' => $row_no,
          ];
        }
        else {
          $table[$row_no]['image'] = [];
        }

        $info_renderable = [
          '#theme' => 'accessory_title',
          '#sku' => $accessory->getSkuFormatted(),
          '#name' => $accessory->getTitle(),
        ];
        $table[$row_no]['info'] = $info_renderable;

        if ($accessory->isFreeOrderQuantity()) {
          $qty_column = [
            '#type' => 'number',
            '#default_value' => !empty($pre_selected_accessories[$row_no]) ? $pre_selected_accessories[$row_no] : 1,
            '#min' => 0,
            '#max' => 99,
            '#step' => 1,
          ];
        }
        else {
          $qty_column = [
            '#type' => 'value',
            '#value' => 1,
            '#suffix' => 1,
          ];
        }
        $table[$row_no]['qty'] = $qty_column;

        $table[$row_no]['selection'] = [
          '#type' => 'checkbox',
          '#default_value' => !empty($pre_selected_accessories[$row_no]) ? 1 : 0,
        ];

      }
      $accessories[] = $table;
    }

    $form['accessories'] = $accessories;
    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['enquiry'] = [
      '#type' => 'submit',
      '#value' => 'Angebot anfordern',
    ];

    $form_state->set('tables_count', count($accessories));
    $form_state->set('trailer', $trailer);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $this->massageAccessoriesValues($form_state);
  }

  /**
   * @inheritDoc
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\unsm_finder\Trailer\RemoteTrailer $trailer */
    $trailer = $form_state->get('trailer');

    /** @var \Drupal\unsm_finder\RemoteTrailerAccessorySelectionStoreInterface $trailer_accessory_selection_store */
    $trailer_accessory_selection_store = \Drupal::service('unsm_finder.remote_trailer_accessory_selection_store');

    $selected_accessories = $form_state->getValue('accessories');
    if (empty($selected_accessories)) {
      $selected_accessories = [];
    }

    $trailer_accessory_selection_store->selectAccessories($trailer, $selected_accessories);
    $form_state->setRedirect('unsm_finder.enquiry.form', ['trailer_id' => $trailer->getId()]);
  }

  /**
   * Massages the submitted form values for the accessories tables on validate.
   *
   * Instead of having different value arrays for every single table, one single
   * 'accessories' result array will be returned, that will already be filtered
   * to only include selected items.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function massageAccessoriesValues(FormStateInterface $form_state) {
    $accessories = [];
    $tables_count = $form_state->get('tables_count');
    if ($tables_count > 0) {
      for ($table_index = 0; $table_index < $tables_count; $table_index++) {
        $values = $form_state->getValue($table_index);
        foreach ($values as $sparepart_id => $row_value) {
          if (empty($row_value['selection']) || empty($row_value['qty'])) {
            continue;
          }
          unset($row_value['selection']);
          $accessories[$sparepart_id] = new AccessoryQuantitySelection($sparepart_id, $row_value['qty']);
        }
        $form_state->unsetValue($table_index);
      }
    }
    $form_state->setValue('accessories', $accessories);
  }

}
