<?php

namespace Drupal\unsm_finder\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\unsm_finder\Form\Value\AccessoryQuantitySelection;
use Drupal\unsm_finder\RemoteTrailerAccessorySelectionStoreInterface;
use Drupal\unsm_finder\Trailer\RemoteTrailer;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides an accessory table form for preselected accessory items only.
 *
 * In contrary to RemoteTrailerAccessoryTableForm, only the preselected items
 * will be rendered and no groupings will be used. Instead of checkboxes, remove
 * actions will be added.
 *
 * @see \Drupal\unsm_finder\Form\RemoteTrailerAccessoryTableForm
 */
class RemoteTrailerPreselectedAccessoryTableForm extends FormBase {

  /**
   * The trailer accessory selection store.
   *
   * @var \Drupal\unsm_finder\RemoteTrailerAccessorySelectionStoreInterface
   */
  protected $trailerAccessorySelectionStore;

  /**
   * Constructs a new TrailerPreselectedAccessoryTableForm object.
   *
   * @param \Drupal\unsm_finder\RemoteTrailerAccessorySelectionStoreInterface $trailer_accessory_selection_store
   *   The trailer accessory selection store.
   */
  public function __construct(RemoteTrailerAccessorySelectionStoreInterface $trailer_accessory_selection_store) {
    $this->trailerAccessorySelectionStore = $trailer_accessory_selection_store;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('unsm_finder.remote_trailer_accessory_selection_store')
    );
  }

  /**
   * Returns a table column mapping from indexes to column names.
   *
   * This is a helper function that can be used in preprocessors to identify
   * column names and e.g. add specific HTML attributes on them.
   * The existence of this workaround is due to the fact, that we found no solid
   * way to integrate both having form fields inside the table and set column
   * attributes. Unless you use the '#rows' property of the table and there
   * inside the renderable style of defining table cells, you don't have to
   * change to set HTML attributes. However, as soon as you start building the
   * render array that way, you lose the ability to properly use form elements.
   *
   * Having this hardcoded static mapping is a good trade off, as we only have
   * to keep this consistent within this class, but gain a reliable way for
   * getting this kind of information from outside. So this is a great way to
   * use inside of template_prepocess_table() implementations.
   */
  public static function getTableColumnMapping() {
    return [
      0 => 'image',
      1 => 'info',
      2 => 'qty',
      3 => 'actions',
    ];
  }

  /**
   * Gets the table column name by its index.
   *
   * See ::getTableColumnMapping() for details about the possible use cases.
   * This function is on top of that to provide a simple and safe way to get the
   * column name of the given column index.

   * @param int $index
   *   The table column index.
   *
   * @return string|null
   *   The column name matching the given index, or NULL if the given value is
   *   unknown or invalid.
   *
   * @see \Drupal\unsm_finder\Form\RemoteTrailerAccessoryTableForm::getTableColumnMapping()
   */
  public static function getTableColumnName($index) {
    $mapping = self::getTableColumnMapping();
    return isset($mapping[$index]) ? $mapping[$index] : NULL;
  }

  /**
   * @inheritDoc
   */
  public function getFormId() {
    return 'remote_trailer_preselected_accessory_table_form';
  }

  /**
   * @inheritDoc
   */
  public function buildForm(array $form, FormStateInterface $form_state, RemoteTrailer $trailer = NULL) {
    if (empty($trailer)) {
      throw new \InvalidArgumentException('The trailer item passed to trailer_preselected_accessory_table_form must not be empty!');
    }
    $accessory_preselection = $this->trailerAccessorySelectionStore->loadSelectedAccessories($trailer);
    if (empty($accessory_preselection)) {
      $form['empty_selection'] = [
        '#type' => 'html_tag',
        '#tag' => 'p',
        // @todo adjust and translate, maybe set a back link to the trailer page as well?
        '#value' => 'Kein Zubehör ausgewählt.',
      ];
      return $form;
    }

    /** @var \Drupal\unsm_finder\Trailer\RemoteAccessory[] $accessories */
    $accessories = [];
    $id_qty_map = [];
    /** @var \Drupal\unsm_finder\Form\Value\AccessoryQuantitySelection $accessory_selection */
    foreach ($accessory_preselection as $accessory_selection) {
      $accessory_id = $accessory_selection->getSparepartId();
      $id_qty_map[$accessory_id] = $accessory_selection->getQty();
      $accessories[$accessory_id] = $trailer->getAccessoryById($accessory_id);
    }

    $table = [
      '#type' => 'table',
      '#attributes' => [
        'class' => ['accessory-table'],
        // data-trailer-accessory-table-form is used to identify the table in
        // preprocessors, etc.
        'data-trailer-preselected-accessory-table-form' => 1,
      ],
    ];

    foreach ($accessories as $accessory) {
      $row_no = $accessory->getId();

      if ($accessory->getThumbnail()) {
        $table[$row_no]['image'] = [
          '#theme' => 'accessory_image',
          '#thumbnail' => $accessory->getThumbnail() ? $accessory->getThumbnail()->toArray() : [],
          '#zoomed' => $accessory->getImage() ? $accessory->getImage()->toArray() : [],
          '#item_id' => $row_no,
        ];
      }
      else {
        $table[$row_no]['image'] = [];
      }

      $info_renderable = [
        '#theme' => 'accessory_title',
        '#sku' => $accessory->getSkuFormatted(),
        '#name' => $accessory->getTitle(),
      ];
      $table[$row_no]['info'] = $info_renderable;

      if ($accessory->isFreeOrderQuantity()) {
        $qty_column = [
          '#type' => 'number',
          '#default_value' => !empty($id_qty_map[$row_no]) ? $id_qty_map[$row_no] : 1,
          '#min' => 0,
          '#max' => 99,
          '#step' => 1,
          '#sparepart_id' => $row_no,
          '#ajax' => [
            'callback' => [$this, 'updateQtyAjax'],
            'event' => 'change',
          ],
        ];
      }
      else {
        $qty_column = [
          '#type' => 'value',
          '#value' => 1,
          '#suffix' => 1,
        ];
      }
      $table[$row_no]['qty'] = $qty_column;

      $table[$row_no]['actions'] = [
        'remove' => [
          '#theme' => 'remove_link',
          '#url' => Url::fromRoute('unsm_finder.offer.remove_accessory',
            ['trailer_id' => $trailer->getId(), 'accessory_id' => $accessory->getId()]),
          '#title' => 'Zubehör entfernen',
        ],
      ];
    }
    $form['accessories'] = $table;

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => 'Mengen aktualisieren',
    ];

    $form_state->set('trailer', $trailer);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $this->massageAccessoriesValues($form_state);
  }

  /**
   * @inheritDoc
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\unsm_finder\Trailer\RemoteTrailer $trailer */
    $trailer = $form_state->get('trailer');

    $selected_accessories = $form_state->getValue('accessories');
    if (empty($selected_accessories)) {
      $selected_accessories = [];
    }

    // Update the quantities.
    $this->trailerAccessorySelectionStore->selectAccessories($trailer, $selected_accessories);
  }

  /**
   * Massages the submitted form values for the accessories table on validate.
   *
   * Instead of having nested arrays of values (quantity), we will convert them
   * to AccessoryQuantitySelection objects instead. This way, we stay
   * homogeneous in our form handling in comparison to the trailer accessory
   * table form used for the detail page.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function massageAccessoriesValues(FormStateInterface $form_state) {
    $accessories = [];
    $values = $form_state->getValue('accessories');
    foreach ($values as $sparepart_id => $row_value) {
      if (empty($row_value['qty'])) {
        continue;
      }
      $accessories[$sparepart_id] = new AccessoryQuantitySelection($sparepart_id, $row_value['qty']);
    }
    $form_state->setValue('accessories', $accessories);
  }

  /**
   * Ajax callback.
   */
  public function updateQtyAjax(array $form, FormStateInterface $form_state) {
    $ajax_response = new AjaxResponse();
    $triggering_element = $form_state->getTriggeringElement();
    if (!empty($triggering_element) && !empty($triggering_element['#sparepart_id'])) {
      /** @var \Drupal\unsm_finder\Trailer\RemoteTrailer $trailer */
      $trailer = $form_state->get('trailer');
      $accessory_selection_update = new AccessoryQuantitySelection($triggering_element['#sparepart_id'], $triggering_element['#value']);
      $this->trailerAccessorySelectionStore->updateQuantity($trailer, $accessory_selection_update);
    }
    return $ajax_response;
  }

}
