<?php

namespace Drupal\unsm_finder\Form\Value;

/**
 * Provides a value object for accessory selection with quantities attached.
 */
final class AccessoryQuantitySelection {

  /**
   * The ID of the given spare part entity (the accessory item).
   *
   * @var int
   */
  protected $sparepartId;

  /**
   * The selected quantity.
   *
   * @var int
   */
  protected $qty;

  /**
   * Constructs a new AccessoryQuantitySelection object.
   *
   * @param string $sparepart_id
   *   The ID of the given spare part entity (the accessory item).
   * @param string $qty
   *   The selected quantity.
   */
  public function __construct($sparepart_id, $qty) {
    $this->sparepartId = (int) $sparepart_id;
    $this->qty = (int) $qty;
  }

  /**
   * Factory method for constructing a new object form a given array.
   *
   * @param array $values
   *   Value array, must contain keys 'sparepart_id' and 'qty'.
   *
   * @return \Drupal\unsm_finder\Form\Value\AccessoryQuantitySelection
   *   The created AccessoryQuantitySelection object.
   *
   * @throws \InvalidArgumentException
   *   If either 'sparepart_id' or 'qty' array key is missing or empty.
   */
  public static function fromArray(array $values) {
    if (empty($values['sparepart_id']) || empty($values['qty'])) {
      throw new \InvalidArgumentException('Cannot create AccessoryQuantitySelection object, "sparepart_id" and "qty" array keys are mandatory!');
    }
    return new static($values['sparepart_id'], $values['qty']);
  }

  /**
   * Gets the spare part (accessory ID).
   *
   * @return int
   *   The spare part (accessory ID).
   */
  public function getSparepartId() {
    return $this->sparepartId;
  }

  /**
   * Gets the selected quantity.
   *
   * @return int
   *   The selected quantity.
   */
  public function getQty() {
    return $this->qty;
  }

  /**
   * Gets the array representation of the object.
   *
   * @return array
   *   The array representation of the object.
   */
  public function toArray() {
    return ['sparepart_id' => $this->sparepartId, 'qty' => $this->qty];
  }

}
