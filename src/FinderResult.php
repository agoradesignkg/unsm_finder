<?php

namespace Drupal\unsm_finder;

/**
 * Value object finder search result.
 */
final class FinderResult {

  /**
   * The result rows.
   *
   * @var \Drupal\unsm_finder\FinderResultRow[]
   */
  protected $rows;

  /**
   * Total number of rows.
   *
   * @var int
   */
  protected $totalRows;

  /**
   * The current page number (0-based).
   *
   * @var int
   */
  protected $currentPage;

  /**
   * Total number of pages.
   *
   * @var int
   */
  protected $pages;

  /**
   * The number of items per page.
   *
   * @var int
   */
  protected $itemsPerPage;

  /**
   * Factory method, instantiating a new FinderResult object.
   *
   * The array structure (of the items) is based on the expectations of the
   * structure returned by UNSINN trailer finder REST web service.
   *
   * @param array $response_body
   *   The JSON response, as returned from UNSINN trailer finder web service.
   * @param string $base_url
   *   An optional base URL for the image and detail urls, if they are provided
   *   with relative urls - they are currently provided with relative urls.
   *
   * @return static
   *   A new FinderResult object, instantiated based on the given values.
   */
  public static function fromArray(array $response_body, $base_url = '') {
    $result = new static();
    $result->rows = [];

    if (!empty($response_body['search_results'])) {
      foreach ($response_body['search_results'] as $row) {
        $result->rows[] = FinderResultRow::fromArray($row, $base_url);
      }
    }

    $result->currentPage = !empty($response_body['current_page']) ? $response_body['current_page'] : 0;
    $result->pages = !empty($response_body['pages']) ? $response_body['pages'] : 1;
    $result->totalRows = !empty($response_body['total_rows']) ? $response_body['total_rows'] : 0;
    $result->itemsPerPage = !empty($response_body['items_per_page']) ? $response_body['items_per_page'] : 0;

    return $result;
  }

  /**
   * Get the result rows.
   *
   * @return \Drupal\unsm_finder\FinderResultRow[]
   *   The result rows.
   */
  public function getRows() {
    return $this->rows;
  }

  /**
   * Get the total number of rows.
   *
   * @return int
   *   Total number of rows.
   */
  public function getTotalRows() {
    return $this->totalRows;
  }

  /**
   * Get the current page number (0-based).
   *
   * @return int
   *   The current page number (0-based).
   */
  public function getCurrentPage() {
    return $this->currentPage;
  }

  /**
   * Get the total number of pages.
   *
   * @return int
   *   Total number of pages.
   */
  public function getPages() {
    return $this->pages;
  }

  /**
   * Get the number of items per page.
   *
   * @return int
   *   The number of items per page.
   */
  public function getItemsPerPage() {
    return $this->itemsPerPage;
  }

}
