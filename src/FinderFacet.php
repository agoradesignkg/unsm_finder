<?php

namespace Drupal\unsm_finder;

/**
 * Value object representing a single finder facet.
 */
final class FinderFacet {

  /**
   * The facet ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The facet label.
   *
   * @var string
   */
  protected $label;

  /**
   * The facet items.
   *
   * @var \Drupal\unsm_finder\FinderFacetItem[]
   */
  protected $items;

  /**
   * Factory method, instantiating a new FinderFacet object with items.
   *
   * The array structure (of the items) is based on the expectations of the
   * structure returned by UNSINN trailer finder REST web service. The label is
   * hardcoded set based on the given facet ID.
   *
   * @param string $id
   *   The facet ID.
   * @param array $items
   *   The facet items as raw data array.
   *
   * @return static
   *   A new FinderFacet object, instantiated based on the given values.
   */
  public static function fromArray($id, array $items) {
    $result = new static();
    $result->id = $id;
    $result->label = FinderFacetsMetadata::getLabel($id);

    // That's not nice that we need a service here inside the value object.
    // But we'll keep it and not rethink architecture at this point now.
    /** @var \Drupal\unsm_finder\FacetsMetadataServiceInterface $facets_metadata_service */
    $facets_metadata_service = \Drupal::service('unsm_finder.facets_metadata_service');

    $result->items = [];
    foreach ($items as $item_raw) {
      if (!is_array($item_raw) || empty($item_raw)) {
        continue;
      }
      if (empty($item_raw['values']['label'])) {
        switch ($id) {
          case 'axles':
            $item_raw['values']['label'] = $item_raw['values']['value'] ? $item_raw['values']['value'] . '-achsig' : '';
            break;

          case 'braked':
            $item_raw['values']['label'] = $item_raw['values']['value'] ? 'Ja' : 'Nein';
            break;

          case 'category':
            $term_id = $item_raw['values']['value'];
            $info = $facets_metadata_service->getCategoryById($term_id);
            if ($info && !empty($info['label'])) {
              $item_raw['values']['label'] = $info['label'];
              $item_raw['values']['thumbnail'] = $info['thumbnail'];
            }
            break;

          case 'field_attr_accessories':
            $term_id = $item_raw['values']['value'];
            $info = $facets_metadata_service->getAccessoryById($term_id);
            if ($info && !empty($info['label'])) {
              $item_raw['values']['label'] = $info['label'];
            }
            break;

          case 'field_trailer_types':
            $term_id = $item_raw['values']['value'];
            $info = $facets_metadata_service->getTrailerTypeById($term_id);
            if ($info && !empty($info['label'])) {
              $item_raw['values']['label'] = $info['label'];
              $item_raw['values']['thumbnail'] = $info['thumbnail'];
            }
            break;

          case 'internal_dimensions_length':
          case 'internal_dimensions_width':
          $item_raw['values']['label'] = $item_raw['values']['value'] . 'mm';
            break;

          case 'total_weight':
            $item_raw['values']['label'] = $item_raw['values']['value'] . 'kg';
            break;
        }
      }
      $result->items[] = FinderFacetItem::fromArray($item_raw);
    }
    return $result;
  }

  /**
   * Get the facet ID.
   *
   * @return string
   *   The facet ID.
   */
  public function getId() {
    return $this->id;
  }

  /**
   * Get the facet label.
   *
   * @return string
   *   The facet label.
   */
  public function getLabel() {
    return $this->label;
  }

  /**
   * Get the facet items.
   *
   * @return \Drupal\unsm_finder\FinderFacetItem[]
   *   The facet items.
   */
  public function getItems() {
    return $this->items;
  }

  /**
   * Returns an array representation of this object.
   *
   * Main purpose is to have an array to access to in our Twig template. It is
   * not a render array per se, but all components are either strings, render
   * arrays or at least compatible to be renderable.
   *
   * @return array
   *   An array representation of this object.
   */
  public function toArray() {
    $result = [
      'id' => $this->id,
      'label' => $this->label,
      'items' => [],
    ];
    foreach ($this->items as $item) {
      $result['items'][] = $item->toArray();
    }
    return $result;
  }

}
