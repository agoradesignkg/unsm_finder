<?php

namespace Drupal\unsm_finder;

/**
 * Value object for operating with trailer finder filter url parameter.
 */
final class FinderFilter {

  /**
   * Remote taxonomy term ID of the trailer type.
   *
   * @var int[]
   */
  protected $trailerTypes = [];

  /**
   * Remote taxonomy term ID of the trailer model.
   *
   * @var int[]
   */
  protected $models = [];

  /**
   * The minimum trailer length (internal dimensions).
   *
   * @var int|null
   */
  protected $lengthMin;

  /**
   * The maximum trailer length (internal dimensions).
   *
   * @var int|null
   */
  protected $lengthMax;

  /**
   * The minimum trailer width (internal dimensions).
   *
   * @var int|null
   */
  protected $widthMin;

  /**
   * The maximum trailer width (internal dimensions).
   *
   * @var int|null
   */
  protected $widthMax;

  /**
   * The minimum trailer weight.
   *
   * @var int|null
   */
  protected $weightMin;

  /**
   * The maximum trailer weight.
   *
   * @var int|null
   */
  protected $weightMax;

  /**
   * The number of axles.
   *
   * @var int[]
   */
  protected $axles = [];

  /**
   * Whether the trailer is braked.
   *
   * @var bool[]
   */
  protected $braked = [];

  /**
   * Remote taxonomy term ID of the trailer's accessories.
   *
   * @var int[]
   */
  protected $accessories = [];

  /**
   * The page. Defaults to 0.
   *
   * @var int
   */
  protected $page = 0;

  /**
   * Factory method, instantiating a new FinderFilter object from array.
   *
   * The array structure is based on the expectations to the query string
   * parameters.
   *
   * @param array $filter
   *   The filter to initialize with.
   *
   * @return static
   *   A new FinderFilter object, instantiated based on the given values.
   */
  public static function fromArray(array $filter) {
    $filter_prepared = [];
    foreach ($filter as $single_filter) {
      list ($filter_key, $filter_value) = explode(':', $single_filter, 2);
      $filter_prepared[$filter_key][] = $filter_value;
    }

    $finder_filter = new static();
    if (!empty($filter_prepared['kategorie'])) {
      $finder_filter->trailerTypes = $filter_prepared['kategorie'];
    }

    if (!empty($filter_prepared['typ'])) {
      $finder_filter->models = $filter_prepared['typ'];
    }

    if (!empty($filter_prepared['laenge'])) {
      $length = self::extractRangeValues($filter_prepared['laenge'][0]);
      if (!empty($length['min'])) {
        $finder_filter->lengthMin = $length['min'];
      }
      if (!empty($length['max'])) {
        $finder_filter->lengthMax = $length['max'];
      }
    }

    if (!empty($filter_prepared['breite'])) {
      $width = self::extractRangeValues($filter_prepared['breite'][0]);
      if (!empty($width['min'])) {
        $finder_filter->widthMin = $width['min'];
      }
      if (!empty($width['max'])) {
        $finder_filter->widthMax = $width['max'];
      }
    }

    if (!empty($filter_prepared['gewicht'])) {
      $weight = self::extractRangeValues($filter_prepared['gewicht'][0]);
      if (!empty($weight['min'])) {
        $finder_filter->weightMin = $weight['min'];
      }
      if (!empty($weight['max'])) {
        $finder_filter->weightMax = $weight['max'];
      }
    }

    if (!empty($filter_prepared['achsen'])) {
      $finder_filter->axles = $filter_prepared['achsen'];
    }

    if (array_key_exists('gebremst', $filter_prepared)) {
      foreach ($filter_prepared['gebremst'] as $braked) {
        $finder_filter->braked[] = $braked ? TRUE : FALSE;
      }
    }

    if (!empty($filter_prepared['zubehoer'])) {
      $finder_filter->accessories = $filter_prepared['zubehoer'];
    }

    return $finder_filter;
  }

  /**
   * Returns an array representation, suitable for query string parameters.
   *
   * @param bool $include_page
   *   Whether to include the page - which isn't actually a filter criteria, but
   *   important for response caching. Defaults to FALSE.
   *
   * @return string[]
   *   An array representation, suitable for query string parameters.
   */
  public function toArray($include_page = FALSE) {
    $result = [];

    if ($this->trailerTypes) {
      foreach ($this->trailerTypes as $trailer_type) {
        $result[] = 'kategorie:' . $trailer_type;
      }
    }

    if ($this->models) {
      foreach ($this->models as $model) {
        $result[] = 'typ:' . $model;
      }
    }

    if ($this->lengthMin && $this->lengthMax) {
      $result[] = sprintf('laenge:(min:%s,max:%s)', $this->lengthMin, $this->lengthMax);
    }

    if ($this->widthMin && $this->widthMax) {
      $result[] = sprintf('breite:(min:%s,max:%s)', $this->widthMin, $this->widthMax);
    }

    if ($this->weightMin && $this->weightMax) {
      $result[] = sprintf('gewicht:(min:%s,max:%s)', $this->weightMin, $this->weightMax);
    }

    if ($this->axles) {
      foreach ($this->axles as $axle) {
        $result[] = 'achsen:' . $axle;
      }
    }

    if (!is_null($this->braked)) {
      foreach ($this->braked as $braked) {
        $result[] = 'gebremst:' . ($braked ? 1 : 0);
      }
    }

    if ($this->accessories) {
      foreach ($this->accessories as $accessory) {
        $result[] = 'zubehoer:' . $accessory;
      }
    }

    if ($include_page) {
      $result[] = 'page:' . $this->page;
    }

    return $result;
  }

  /**
   * Extracts the min and max value of the given range value string.
   *
   * @param string $range_value_string
   *   A range value string, e.g.: '(min:6060,max:9660)'.
   *
   * @return int[]
   *   An array containing two values, min and max. If the given string is empty
   *   or invalid, both values will be set to NULL.
   */
  protected static function extractRangeValues($range_value_string) {
    $result = [
      'min' => NULL,
      'max' => NULL,
    ];
    if (!empty($range_value_string)) {
      $matches = [];
      if (preg_match('/\(min:(\d+),max:(\d+)\)/', $range_value_string, $matches)) {
        $result['min'] = $matches[1];
        $result['max'] = $matches[2];
      }
    }
    return $result;
  }

  /**
   * @return int[]
   */
  public function getTrailerTypes() {
    return $this->trailerTypes;
  }

  /**
   * @param int[] $trailerTypes
   */
  public function setTrailerTypes($trailerTypes) {
    $this->trailerTypes = $trailerTypes;
  }

  /**
   * @return int[]
   */
  public function getModels() {
    return $this->models;
  }

  /**
   * @param int[] $models
   */
  public function setModels($models) {
    $this->models = $models;
  }

  /**
   * @return int
   */
  public function getPage() {
    return $this->page;
  }

  /**
   * @param int $page
   */
  public function setPage($page) {
    $this->page = $page;
  }

}
