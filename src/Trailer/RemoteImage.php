<?php

namespace Drupal\unsm_finder\Trailer;

use Drupal\Core\Url;

/**
 * Value object for representing a remote image, having url and dimensions.
 */
final class RemoteImage {

  /**
   * The image url.
   *
   * @var \Drupal\Core\Url
   */
  protected $url;

  /**
   * The width.
   *
   * @var int
   */
  protected $width;

  /**
   * The height.
   *
   * @var int
   */
  protected $height;

  /**
   * Factory method, instantiating a new RemoteImage object.
   *
   * The array structure (of the items) is based on the expectations of the
   * structure returned by UNSINN REST web service.
   *
   * @param array $values
   *   Part of the JSON response, as returned from UNSINN web service.
   *
   * @return static
   *   A new RemoteImage object, instantiated based on the given values.
   */
  public static function fromArray(array $values) {
    $result = new static();
    $result->url = $values['url'] instanceof Url ? $values['url'] : Url::fromUri($values['url']);

    if (!empty($values['width'])) {
      $result->width = $values['width'];
    }

    if (!empty($values['height'])) {
      $result->height = $values['height'];
    }

    return $result;
  }

  /**
   * @return \Drupal\Core\Url
   */
  public function getUrl() {
    return $this->url;
  }

  /**
   * @return int
   */
  public function getWidth() {
    return $this->width;
  }

  /**
   * @return int
   */
  public function getHeight() {
    return $this->height;
  }

  /**
   * @return array
   */
  public function toArray() {
    return [
      'url' => $this->url ? $this->url->toString() : '',
      'width' => $this->width,
      'height' => $this->height,
    ];
  }

}
