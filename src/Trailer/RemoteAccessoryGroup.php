<?php

namespace Drupal\unsm_finder\Trailer;

/**
 * Value object for remote accessory groups.
 */
final class RemoteAccessoryGroup {

  /**
   * The accessory title.
   *
   * @var string
   */
  protected $title;

  /**
   * The accessories - keyed by accessory ID.
   *
   * @var \Drupal\unsm_finder\Trailer\RemoteAccessory[]
   */
  protected $accessories;

  /**
   * Factory method, instantiating a new RemoteAccessoryGroup object.
   *
   * The array structure (of the items) is based on the expectations of the
   * structure returned by UNSINN REST web service.
   *
   * @param array $values
   *   Part of the JSON response, as returned from UNSINN web service.
   *
   * @return static
   *   A new RemoteAccessoryGroup object, instantiated based on the given values.
   */
  public static function fromArray(array $values) {
    $result = new static();
    $result->title = isset($values['group']) ? $values['group'] : '';

    $result->accessories = [];
    if (!empty($values['accessories'])) {
      foreach ($values['accessories'] as $accessory) {
        $item = RemoteAccessory::fromArray($accessory);
        $result->accessories[$item->getId()] = $item;
      }
    }

    return $result;
  }

  /**
   * @return string
   */
  public function getTitle() {
    return $this->title;
  }

  /**
   * @return \Drupal\unsm_finder\Trailer\RemoteAccessory[]
   */
  public function getAccessories() {
    return $this->accessories;
  }

  /**
   * @return bool
   */
  public function hasAccessories() {
    return !empty($this->accessories);
  }

  /**
   * @param int $id
   *   The accessory ID.
   *
   * @return \Drupal\unsm_finder\Trailer\RemoteAccessory|null
   */
  public function getAccessoryById($id) {
    return isset($this->accessories[$id]) ? $this->accessories[$id] : NULL;
  }

}
