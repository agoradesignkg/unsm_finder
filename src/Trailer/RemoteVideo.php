<?php

namespace Drupal\unsm_finder\Trailer;

use Drupal\Core\Url;

/**
 * Value object for remote images, having thumbnail and url.
 */
final class RemoteVideo {

  /**
   * The YouTube ID - if it's an YouTube video at all.
   *
   * @var string|null
   */
  protected $youtubeId;

  /**
   * The video url.
   *
   * @var \Drupal\Core\Url
   */
  protected $url;

  /**
   * RemoteVideo constructor.
   *
   * @param \Drupal\Core\Url|string $url
   *   The url - can be an Url object, or a string.
   */
  public function __construct($url) {
    $url_string = $url instanceof Url ? $url->toString() : $url;
    $this->youtubeId = $this->getYoutubeIdFromInput($url_string);
    $url_options = [];
    if ($this->youtubeId) {
      $url_string = sprintf('https://www.youtube.com/embed/%s', $this->youtubeId);
      $url_options['query'] = ['rel' => 0, 'showinfo' => 0];
    }
    $this->url = Url::fromUri($url_string, $url_options);
  }

  /**
   * @return \Drupal\Core\Url
   */
  public function getUrl() {
    return $this->url;
  }

  /**
   * @return bool
   */
  public function isValid() {
    return !empty($this->url);
  }

  /**
   * @return string|null
   */
  public function getYoutubeId() {
    return $this->youtubeId;
  }

  /**
   * {@inheritdoc}
   */
  protected function getYoutubeIdFromInput($input) {
    $matches = [];
    if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $input, $matches)) {
      return $matches[1];
    }
    return NULL;
  }

}
