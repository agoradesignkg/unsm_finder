<?php

namespace Drupal\unsm_finder\Trailer;

/**
 * Value object for remote trailers.
 */
final class RemoteTrailer {

  /**
   * The trailer ID.
   *
   * @var int
   */
  protected $id;

  /**
   * The trailer title.
   *
   * @var string
   */
  protected $title;

  /**
   * The model name.
   *
   * @var string
   */
  protected $model;

  /**
   * The model ID.
   *
   * @var int
   */
  protected $modelId;

  /**
   * The trailer type IDs.
   *
   * @var int[]
   */
  protected $trailerTypes;

  /**
   * The trailer entity bundle.
   *
   * @var string
   */
  protected $bundle;

  /**
   * The formatted total weight.
   *
   * @var string
   */
  protected $totalWeight;

  /**
   * The formatted internal dimensions.
   *
   * @var string
   */
  protected $internalDimensions;

  /**
   * The formatted loading height.
   *
   * @var string
   */
  protected $loadingHeight;

  /**
   * The formatted load capacity.
   *
   * @var string
   */
  protected $loadCapacity;

  /**
   * The description text (contains HTML).
   *
   * @var string
   */
  protected $description;

  /**
   * The image.
   *
   * @var \Drupal\unsm_finder\Trailer\RemoteImage|null
   */
  protected $image;

  /**
   * The downloads.
   *
   * @var \Drupal\unsm_finder\Trailer\RemoteDownload[]
   */
  protected $downloads;

  /**
   * The accessory groups.
   *
   * @var \Drupal\unsm_finder\Trailer\RemoteAccessoryGroup[]
   */
  protected $accessoryGroups;

  /**
   * The "possibilities" gallery.
   *
   * @var \Drupal\unsm_finder\Trailer\RemoteImages[]
   */
  protected $gallery;

  /**
   * Video urls.
   *
   * @var \Drupal\unsm_finder\Trailer\RemoteVideo[]
   */
  protected $videos;

  /**
   * RemoteTrailer constructor.
   *
   * @param int $id
   *   The trailer ID.
   */
  public function __construct($id) {
    $this->id = $id;
  }

  /**
   * Factory method, instantiating a new RemoteTrailer object.
   *
   * The array structure (of the items) is based on the expectations of the
   * structure returned by UNSINN REST web service.
   *
   * @param array $response_body
   *   The JSON response, as returned from UNSINN web service.
   *
   * @return static
   *   A new RemoteTrailer object, instantiated based on the given values.
   */
  public static function fromArray(array $response_body) {
    $result = new static($response_body['id']);
    $result->title = isset($response_body['title']) ? $response_body['title'] : '';
    $result->model = isset($response_body['model']) ? $response_body['model'] : '';
    $result->modelId = isset($response_body['model_id']) ? $response_body['model_id'] : '';
    $result->trailerTypes = isset($response_body['trailer_types']) ? $response_body['trailer_types'] : [];
    $result->bundle = isset($response_body['bundle']) ? $response_body['bundle'] : '';
    $result->description = isset($response_body['description']) ? trim($response_body['description']) : '';
    $result->internalDimensions = isset($response_body['internal_dimensions']) ? $response_body['internal_dimensions'] : '';
    $result->totalWeight = isset($response_body['total_weight']) ? $response_body['total_weight'] : '';
    $result->loadingHeight = isset($response_body['loading_height']) ? $response_body['loading_height'] : '';
    $result->loadCapacity = isset($response_body['load_capacity']) ? $response_body['load_capacity'] : '';

    if (!empty($response_body['image'])) {
      $result->image = RemoteImage::fromArray($response_body['image']);
    }

    $result->downloads = [];
    if (!empty($response_body['downloads'])) {
      foreach ($response_body['downloads'] as $download) {
        $result->downloads[] = RemoteDownload::fromArray($download);
      }
    }

    $result->accessoryGroups = [];
    if (!empty($response_body['accessories'])) {
      foreach ($response_body['accessories'] as $accessory_group) {
        $result->accessoryGroups[] = RemoteAccessoryGroup::fromArray($accessory_group);
      }
    }

    $result->gallery = [];
    if (!empty($response_body['gallery'])) {
      foreach ($response_body['gallery'] as $gallery_item) {
        $result->gallery[] = RemoteImages::fromArray($gallery_item);
      }
    }

    $result->videos = [];
    if (!empty($response_body['videos'])) {
      foreach ($response_body['videos'] as $video) {
        $result->videos[] = new RemoteVideo($video);
      }
    }

    return $result;
  }

  /**
   * @return int
   */
  public function getId() {
    return $this->id;
  }

  /**
   * @return string
   */
  public function getTitle() {
    return $this->title;
  }

  /**
   * @return string
   */
  public function getModel() {
    return $this->model;
  }

  /**
   * @return int
   */
  public function getModelId() {
    return $this->modelId;
  }

  /**
   * @return int[]
   */
  public function getTrailerTypes() {
    return $this->trailerTypes;
  }

  /**
   * @return string
   */
  public function getFullName() {
    $name = '';
    if ($this->model) {
      $name = $this->model . ' ';
    }
    $name .= $this->title;
    return $name;
  }

  /**
   * @return string
   */
  public function getBundle() {
    return $this->bundle;
  }

  /**
   * @return string
   */
  public function getTotalWeight() {
    return $this->totalWeight;
  }

  /**
   * @return string
   */
  public function getInternalDimensions() {
    return $this->internalDimensions;
  }

  /**
   * @return string
   */
  public function getLoadingHeight() {
    return $this->loadingHeight;
  }

  /**
   * @return string
   */
  public function getLoadCapacity() {
    return $this->loadCapacity;
  }

  /**
   * @return string
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * @return \Drupal\unsm_finder\Trailer\RemoteImage|null
   */
  public function getImage() {
    return $this->image;
  }

  /**
   * @return \Drupal\unsm_finder\Trailer\RemoteDownload[]
   */
  public function getDownloads() {
    return $this->downloads;
  }

  /**
   * @return \Drupal\unsm_finder\Trailer\RemoteAccessoryGroup[]
   */
  public function getAccessoryGroups() {
    return $this->accessoryGroups;
  }

  /**
   * @return bool
   */
  public function hasAccessories() {
    if (!empty($this->accessoryGroups)) {
      foreach ($this->getAccessoryGroups() as $accessory_group) {
        if ($accessory_group->hasAccessories()) {
          return TRUE;
        }
      }
    }
    return FALSE;
  }

  /**
   * @return \Drupal\unsm_finder\Trailer\RemoteImages[]
   */
  public function getGallery() {
    return $this->gallery;
  }

  /**
   * @return \Drupal\unsm_finder\Trailer\RemoteVideo[]
   */
  public function getVideos() {
    return $this->videos;
  }

  /**
   * @param int $id
   *   The accessory ID.
   *
   * @return \Drupal\unsm_finder\Trailer\RemoteAccessory|null
   */
  public function getAccessoryById($id) {
    foreach ($this->accessoryGroups as $accessory_group) {
      if ($accessory = $accessory_group->getAccessoryById($id)) {
        return $accessory;
      }
    }
    return NULL;
  }

}
