<?php

namespace Drupal\unsm_finder\Trailer;

use Drupal\Core\Url;

/**
 * Value object for remote accessories.
 */
final class RemoteAccessory {

  /**
   * The accessory ID.
   *
   * @var int
   */
  protected $id;

  /**
   * The accessory title.
   *
   * @var string
   */
  protected $title;

  /**
   * The SKU.
   *
   * @var string
   */
  protected $sku;

  /**
   * The formatted SKU.
   *
   * @var string
   */
  protected $skuFormatted;

  /**
   * Whether or not the order quantity can be chosen.
   *
   * @var bool
   */
  protected $freeOrderQuantity;

  /**
   * The (zoomed) image.
   *
   * @var \Drupal\unsm_finder\Trailer\RemoteImage|null
   */
  protected $image;

  /**
   * The thumbnail.
   *
   * @var \Drupal\unsm_finder\Trailer\RemoteImage|null
   */
  protected $thumbnail;

  /**
   * Factory method, instantiating a new RemoteAccessory object.
   *
   * The array structure (of the items) is based on the expectations of the
   * structure returned by UNSINN REST web service.
   *
   * @param array $values
   *   Part of the JSON response, as returned from UNSINN web service.
   *
   * @return static
   *   A new RemoteAccessory object, instantiated based on the given values.
   */
  public static function fromArray(array $values) {
    $result = new static();
    $result->id = isset($values['id']) ? $values['id'] : '';
    $result->title = isset($values['title']) ? $values['title'] : '';
    $result->sku = isset($values['sku']) ? $values['sku'] : '';
    $result->skuFormatted = isset($values['sku_formatted']) ? $values['sku_formatted'] : '';
    $result->freeOrderQuantity = isset($values['free_order_quantity']) ? (bool) $values['free_order_quantity'] : FALSE;
    if (!empty($values['image'])) {
      $result->image = RemoteImage::fromArray($values['image']);
    }
    if (!empty($values['thumbnail'])) {
      $result->thumbnail = RemoteImage::fromArray($values['thumbnail']);
    }

    return $result;
  }

  /**
   * @return int
   */
  public function getId() {
    return $this->id;
  }

  /**
   * @return string
   */
  public function getTitle() {
    return $this->title;
  }

  /**
   * @return string
   */
  public function getSku() {
    return $this->sku;
  }

  /**
   * @return string
   */
  public function getSkuFormatted() {
    return $this->skuFormatted;
  }

  /**
   * @return bool
   */
  public function isFreeOrderQuantity() {
    return $this->freeOrderQuantity;
  }

  /**
   * @return \Drupal\unsm_finder\Trailer\RemoteImage|null
   */
  public function getImage() {
    return $this->image;
  }

  /**
   * @return \Drupal\unsm_finder\Trailer\RemoteImage|null
   */
  public function getThumbnail() {
    return $this->thumbnail;
  }

}
