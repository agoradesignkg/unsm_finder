<?php

namespace Drupal\unsm_finder\Trailer;

use Drupal\Core\Url;

/**
 * Value object for remote downloads.
 */
final class RemoteDownload {

  /**
   * The download title.
   *
   * @var string
   */
  protected $title;

  /**
   * The download url.
   *
   * @var \Drupal\Core\Url
   */
  protected $url;

  /**
   * The thumbnail url.
   *
   * @var \Drupal\Core\Url|null
   */
  protected $thumbnail;

  /**
   * Factory method, instantiating a new RemoteDownload object.
   *
   * The array structure (of the items) is based on the expectations of the
   * structure returned by UNSINN REST web service.
   *
   * @param array $values
   *   The JSON response, as returned from UNSINN web service.
   *
   * @return static
   *   A new RemoteDownload object, instantiated based on the given values.
   */
  public static function fromArray(array $values) {
    $result = new static();
    $result->title = isset($values['title']) ? $values['title'] : '';
    if (!empty($values['url'])) {
      $result->url = $values['url'] instanceof Url ? $values['url'] : Url::fromUri($values['url']);
    }
    if (!empty($values['thumbnail'])) {
      $result->thumbnail = $values['thumbnail'] instanceof Url ? $values['thumbnail'] : Url::fromUri($values['thumbnail']);
    }

    return $result;
  }

  /**
   * @return string
   */
  public function getTitle() {
    return $this->title;
  }

  /**
   * @return \Drupal\Core\Url
   */
  public function getUrl() {
    return $this->url;
  }

  /**
   * @return \Drupal\Core\Url|null
   */
  public function getThumbnail() {
    return $this->thumbnail;
  }

}
