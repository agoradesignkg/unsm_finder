<?php

namespace Drupal\unsm_finder\Trailer;

/**
 * Value object for remote images, having thumbnail and zoomed urls.
 *
 * This is bundling different styles together, each pointing to a RemoteImage
 * object.
 */
final class RemoteImages {

  /**
   * The detail (zoomed) image.
   *
   * @var \Drupal\unsm_finder\Trailer\RemoteImage
   */
  protected $main;

  /**
   * The thumbnail image.
   *
   * @var \Drupal\unsm_finder\Trailer\RemoteImage|null
   */
  protected $thumbnail;

  /**
   * Factory method, instantiating a new RemoteImages object.
   *
   * The array structure (of the items) is based on the expectations of the
   * structure returned by UNSINN REST web service.
   *
   * @param array $values
   *   Part of the JSON response, as returned from UNSINN web service.
   *
   * @return static
   *   A new RemoteImages object, instantiated based on the given values.
   */
  public static function fromArray(array $values) {
    $result = new static();

    if (!empty($values['main'])) {
      $result->main = RemoteImage::fromArray($values['main']);
    }
    elseif (!empty($values['zoomed'])) {
      $result->main = RemoteImage::fromArray($values['zoomed']);
    }
    if (!empty($values['thumbnail'])) {
      $result->thumbnail = RemoteImage::fromArray($values['thumbnail']);
    }
    return $result;
  }

  /**
   * @return \Drupal\unsm_finder\Trailer\RemoteImage
   */
  public function getMain() {
    return $this->main;
  }

  /**
   * @return \Drupal\unsm_finder\Trailer\RemoteImage|null
   */
  public function getThumbnail() {
    return $this->thumbnail;
  }

  /**
   * @return bool
   */
  public function isValid() {
    return !empty($this->main);
  }

  /**
   * @return array
   */
  public function toArray() {
    return [
      'main' => $this->main ? $this->main->toArray() : [],
      'thumbnail' => $this->thumbnail ? $this->thumbnail->toArray() : [],
    ];
  }

}
